This is a java port of TinyJS, code named frenchvanilla, intended to be an extendable scripting interface for
run-time defined processes within a java sandbox. The codebase retains the original MIT liscense, and all of the logic; only
the syntax has been adapted, along with seperating class files. There are runtime errors. TODOs may be marked where something 
needs revisiting, trivial c statements may also be commented beside a java version. Most changes were using Objects.isNull to 
substitute *void=0 assigned objects, and a linked list implementation that needs to be java'fied.

Original readme:
This project aims to be an extremely simple (~2000 line) JavaScript interpreter, meant for inclusion in applications that require a simple, familiar script language that can be included with no dependencies other than normal C++ libraries. It currently consists of two source files: one containing the interpreter, another containing built-in functions such as String.substring.

TinyJS is not designed to be fast or full-featured. However it is great for scripting simple behaviour, or loading & saving settings.

I make absolutely no guarantees that this is compliant to JavaScript/EcmaScript standard. In fact I am sure it isn't. However I welcome suggestions for changes that will bring it closer to compliance without overly complicating the code, or useful test cases to add to the test suite.

Currently TinyJS supports:

Variables, Arrays, Structures
JSON parsing and output
Functions
Calling C/C++ code from JavaScript
Objects with Inheritance (not fully implemented)
Please see CodeExamples for examples of code that works...

For a list of known issues, please see the comments at the top of the TinyJS.cpp file, as well as the GitHub issues

TinyJS is released under an MIT licence.

Internal Structure
TinyJS uses a Recursive Descent Parser, so there is no 'Parser Generator' required. It does not compile to an intermediate code, and instead executes directly from source code. This makes it quite fast for code that is executed infrequently, and slow for loops.

Variables, Arrays and Objects are stored in a simple linked list tree structure (42tiny-js uses a C++ Map). This is simple, but relatively slow for large structures or arrays.