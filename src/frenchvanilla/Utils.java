package frenchvanilla;

public class Utils {
	
	//alot of this file can be replaced with javabits
	
	public static boolean isWhitespace(char ch) {
	    return (ch==' ') || (ch=='\t') || (ch=='\n') || (ch=='\r');
	}

	public static boolean isNumeric(char ch) {
	    return (ch>='0') && (ch<='9');
	}
	public static boolean isNumber(String str) {
	    for (int i=0;i<str.length();i++)
	      if (!isNumeric(str.charAt(i))) return false;
	    return true;
	}
	public static boolean isHexadecimal(char ch) {
	    return ((ch>='0') && (ch<='9')) ||
	           ((ch>='a') && (ch<='f')) ||
	           ((ch>='A') && (ch<='F'));
	}
	public static boolean isAlpha(char ch) {
	    return ((ch>='a') && (ch<='z')) || ((ch>='A') && (ch<='Z')) || ch=='_';
	}

	public static boolean isIDString(final String s) {
		char c=s.charAt(0);
		if (!isAlpha(c))
	        return false;
	    int i=0;
	    while (c != '\0') {
	        if (!(isAlpha(c) || isNumeric(c)))
	            return false;
	        c=s.charAt(++i);
	    }
	    return true;
	}

	public static void replace(String str, String textFrom, final String textTo) {
	    int p = str.indexOf(textFrom, 0);
	    while (p != (-1)) {
	        str = str.substring(0, p) + textTo + str.substring(p+1);
	        p = str.indexOf(textFrom, p+textTo.length());
	    }
	}

	public static int atoi(String str) {
		if (str == null || str.length() < 1)
			return 0;
	 
		// trim white spaces
		str = str.trim();
	 
		char flag = '+';
	 
		// check negative or positive
		int i = 0;
		if (str.charAt(0) == '-') {
			flag = '-';
			i++;
		} else if (str.charAt(0) == '+') {
			i++;
		}
		// use double to store result
		double result = 0;
	 
		// calculate value
		while (str.length() > i && str.charAt(i) >= '0' && str.charAt(i) <= '9') {
			result = result * 10 + (str.charAt(i) - '0');
			i++;
		}
	 
		if (flag == '-')
			result = -result;
	 
		// handle max and min
		if (result > Integer.MAX_VALUE)
			return Integer.MAX_VALUE;
	 
		if (result < Integer.MIN_VALUE)
			return Integer.MIN_VALUE;
	 
		return (int) result;
	}
	
	/// convert the given string into a quoted string suitable for javascript
	public static String getJSString(final String str) {
	    String nStr = str;
	    for (int i=0;i<nStr.length();i++) {
	      String replaceWith = "";
	      boolean replace = true;

	      switch (nStr.charAt(i)) {
	        case '\\': replaceWith = "\\\\"; break;
	        case '\n': replaceWith = "\\n"; break;
	        case '\r': replaceWith = "\\r"; break;
	        case 0x07: replaceWith = "\\a"; break;
	        case '"': replaceWith = "\\\""; break;
	        default: {
	          int nCh = ((int)nStr.charAt(i)) &0xFF;
	          if (nCh<32 || nCh>127) {
	            // sprintf_s(buffer, 5, "\\x%02X", nCh);
	            replaceWith = String.valueOf(nCh);
	          } else replace=false;
	        }
	      }

	      if (replace) {
	        nStr = nStr.substring(0, i) + replaceWith + nStr.substring(i+1);
	        i += replaceWith.length()-1;
	      }
	    }
	    return "\"" + nStr + "\"";
	}

	/** Is the string alphanumeric */
	public static boolean isAlphaNum(final String str) {
	    if (str.length()==0) return true;
	    if (!isAlpha(str.charAt(0))) return false;
	    for (int i=0;i<str.length();i++)
	      if (!(isAlpha(str.charAt(i)) || isNumeric(str.charAt(i))))
	        return false;
	    return true;
	}
}
