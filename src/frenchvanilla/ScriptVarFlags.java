package frenchvanilla;

public class ScriptVarFlags {

	public static final String SCRIPTSTR_FUNCTION = "FUNCTION ";
	public static final String SCRIPTSTR_OBJECT = "OBJECT ";
	public static final String SCRIPTSTR_ARRAY = "ARRAY ";
	public static final String SCRIPTSTR_NATIVE = "NATIVE ";
	public static final String SCRIPTSTR_DOUBLE = "DOUBLE ";
	public static final String SCRIPTSTR_INTEGER = "INTEGER ";
	public static final String SCRIPTSTR_STRING = "STRING ";
	
	public static final int SCRIPTVAR_UNDEFINED = 0;
	public static final int SCRIPTVAR_FUNCTION = 1;
	public static final int SCRIPTVAR_OBJECT = 2;
	public static final int SCRIPTVAR_ARRAY = 4;
	public static final int SCRIPTVAR_DOUBLE = 8;  // floating point double
	public static final int SCRIPTVAR_INTEGER = 16; // integer number
	public static final int SCRIPTVAR_STRING = 32; // string
	public static final int SCRIPTVAR_NULL = 64; // it seems null is its own data type

	public static final int SCRIPTVAR_NATIVE = 128; // to specify this is a native function
	public static final int SCRIPTVAR_NUMERICMASK = (SCRIPTVAR_NULL |
                            SCRIPTVAR_DOUBLE |
                            SCRIPTVAR_INTEGER);
	public static final int SCRIPTVAR_VARTYPEMASK = (SCRIPTVAR_DOUBLE |
                            SCRIPTVAR_INTEGER |
                            SCRIPTVAR_STRING |
                            SCRIPTVAR_FUNCTION |
                            SCRIPTVAR_OBJECT |
                            SCRIPTVAR_ARRAY |
                            SCRIPTVAR_NULL);
	
}
