package frenchvanilla;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;

public class TinyJS {
	// debug flag
	final static boolean TINYJS_CALL_STACK = false;
	
	public static int TINYJS_LOOP_MAX_ITERATIONS = 8192;
	public static String TINYJS_NULL_VAR = "null";
	public static String TINYJS_UNDEF_VAR = "undefined";
	public static String TINYJS_RETURN_VAR = "return";
	public static String TINYJS_PROTOTYPE_CLASS = "prototype";
	public static String TINYJS_TEMP_NAME = "";
	public static String TINYJS_BLANK_DATA = "";
	
	public ScriptVar root;   /// root of symbol table
	private ScriptLex l;             /// current lexer
	private List <ScriptVar> scopes; /// stack of scopes when parsing
	//if (TINYJS_CALL_STACK)
	    List <String> call_stack; /// Names of places called so we can show when erroring

    ScriptVar stringClass; /// Built in string class
    ScriptVar objectClass; /// Built in object class
    ScriptVar arrayClass; /// Built in array class
	    
   
    public TinyJS() {
        l = null;
        root = (new ScriptVar(TINYJS_BLANK_DATA, ScriptVarFlags.SCRIPTVAR_OBJECT)).ref();
        // Add built-in classes
        stringClass = (new ScriptVar(TINYJS_BLANK_DATA, ScriptVarFlags.SCRIPTVAR_OBJECT)).ref();
        arrayClass = (new ScriptVar(TINYJS_BLANK_DATA, ScriptVarFlags.SCRIPTVAR_OBJECT)).ref();
        objectClass = (new ScriptVar(TINYJS_BLANK_DATA, ScriptVarFlags.SCRIPTVAR_OBJECT)).ref();
        root.addChild("String", stringClass);
        root.addChild("Array", arrayClass);
        root.addChild("Object", objectClass);
    }

    protected void finalize() {
        //ASSERT(!l);
        scopes.clear();
        stringClass.unref();
        arrayClass.unref();
        objectClass.unref();
        root.unref();

    // #if DEBUG_MEMORY show_allocated();
    }

    public void trace() {
        root.trace();
    }

    
	/* Frees the given link IF it isn't owned by anything else */
	public void CLEAN(ScriptVarLink x) { ScriptVarLink v = x; if (!Objects.isNull(v) && !v.owned) { v.finalize(); } }
	/* Create a LINK to point to VAR and free the old link.
	 * BUT this is more clever - it tries to keep the old link if it's not owned to save allocations */
	public void CREATE_LINK(ScriptVarLink LINK, ScriptVar VAR) { if (Objects.isNull(LINK) || LINK.owned) LINK = new ScriptVarLink(VAR); else LINK.replaceWith(VAR); }
	
    public void execute(final String code) throws Exception {
        ScriptLex oldLex = l;
        List<ScriptVar> oldScopes = scopes;
        l = new ScriptLex(code);
    if(TINYJS_CALL_STACK)
        call_stack.clear();
    
        scopes.clear();
        //scopes.push_back(root);
        scopes.add(root);
        try {
            boolean execute = true;
            while (!Objects.isNull(l.tk)) statement(execute);
        } catch (Exception e) {
            String msg = "Error " + e.getMessage();
            if(TINYJS_CALL_STACK)
            	for (int i=(int)call_stack.size()-1;i>=0;i--)
            		msg += "\n" + i + ": " + call_stack.get(i);
    
            msg += " at " + l.getPosition();
            l.finalize();
            l = oldLex;

            throw new Exception(msg);
        }
        l.finalize();
        l = oldLex;
        scopes = oldScopes;
    }

    public ScriptVarLink evaluateComplex(final String code) throws Exception {
        ScriptLex oldLex = l;
        List<ScriptVar> oldScopes = scopes;

        l = new ScriptLex(code);
    if(TINYJS_CALL_STACK)
        call_stack.clear();
    
        scopes.clear();
        scopes.add(root);
        ScriptVarLink v = null;
        try {
            boolean execute = true;
            do {
              CLEAN(v);
              v = base(execute);
              if (l.tk!=LexTypes.LEX_EOF) l.match(';');
            } while (l.tk!=LexTypes.LEX_EOF);
        } catch (Exception e) {
          String msg = "Error " + e.getMessage();
    if(TINYJS_CALL_STACK)
          for (int i=(int)call_stack.size()-1;i>=0;i--)
            msg += "\n" + i + ": " + call_stack.get(i);
    
          msg += " at " + l.getPosition();
          l.finalize();;
          l = oldLex;

            throw new Exception(msg);
        }
        l.finalize();;
        l = oldLex;
        scopes = oldScopes;

        if (!Objects.isNull(v)) {
            ScriptVarLink r = v;
            CLEAN(v);
            return r;
        }
        // return undefined...
        return new ScriptVarLink(new ScriptVar());
    }

    String evaluate(final String code) throws Exception {
        return evaluateComplex(code).var.getString();
    }

    void parseFunctionArguments(ScriptVar funcVar) {
      l.match('(');
      while (l.tk!=')') {
          funcVar.addChildNoDup(l.tkStr);
          l.match(LexTypes.LEX_ID);
          if (l.tk!=')') l.match(',');
      }
      l.match(')');
    }

    public void addNative(final String funcDesc, Method JSCallback_ptr, String userdata) {
        ScriptLex oldLex = l;
        l = new ScriptLex(funcDesc);

        ScriptVar base = root;

        l.match(LexTypes.LEX_R_FUNCTION);
        String funcName = l.tkStr;
        l.match(LexTypes.LEX_ID);
        /* Check for dots, we might want to do something like function String.substring ... */
        while (l.tk == '.') {
          l.match('.');
          ScriptVarLink link = base.findChild(funcName);
          // if it doesn't exist, make an object class
          if (Objects.isNull(link)) link = base.addChild(funcName, new ScriptVar(TINYJS_BLANK_DATA, ScriptVarFlags.SCRIPTVAR_OBJECT));
          base = link.var;
          funcName = l.tkStr;
          l.match(LexTypes.LEX_ID);
        }

        ScriptVar funcVar = new ScriptVar(TINYJS_BLANK_DATA, ScriptVarFlags.SCRIPTVAR_FUNCTION | ScriptVarFlags.SCRIPTVAR_NATIVE);
        funcVar.setCallback(JSCallback_ptr, userdata);
        parseFunctionArguments(funcVar);
        l.finalize();
        l = oldLex;

        base.addChild(funcName, funcVar);
    }

    ScriptVarLink parseFunctionDefinition() {
      // actually parse a function...
      l.match(LexTypes.LEX_R_FUNCTION);
      String funcName = TINYJS_TEMP_NAME;
      /* we can have functions without names */
      if (l.tk==LexTypes.LEX_ID) {
        funcName = l.tkStr;
        l.match(LexTypes.LEX_ID);
      }
      ScriptVarLink funcVar = new ScriptVarLink(new ScriptVar(TINYJS_BLANK_DATA, ScriptVarFlags.SCRIPTVAR_FUNCTION), funcName);
      parseFunctionArguments(funcVar.var);
      int funcBegin = l.tokenStart;
      boolean noexecute = false;
      block(noexecute);
      funcVar.var.strData = l.getSubString(funcBegin);
      return funcVar;
    }

    /** Handle a function call (assumes we've parsed the function name and we're
     * on the start bracket). 'parent' is the object that contains this method,
     * if there was one (otherwise it's just a normnal function).
     * @throws Exception 
     */
    
    ScriptVarLink functionCall(boolean execute, ScriptVarLink function, ScriptVar parent){
      if (execute) {
        if (!function.var.isFunction()) {
        	;// TODO: make this sit and spin instead of crashing
            //throw new Exception("Expecting '" + function.name + "' to be a function");
        }
        l.match('(');
        // create a new symbol table entry for execution of this function
        ScriptVar functionRoot = new ScriptVar(TINYJS_BLANK_DATA, ScriptVarFlags.SCRIPTVAR_FUNCTION);
        if (!Objects.isNull(parent))
          functionRoot.addChildNoDup("this", parent);
        // grab in all parameters
        ScriptVarLink v = function.var.firstChild;
        while (!Objects.isNull(v)) {
            ScriptVarLink value = base(execute);
            if (execute) {
                if (value.var.isBasic()) {
                  // pass by value
                  functionRoot.addChild(v.name, value.var.deepCopy());
                } else {
                  // pass by reference
                  functionRoot.addChild(v.name, value.var);
                }
            }
            CLEAN(value);
            if (l.tk!=')') l.match(',');
            v = v.nextSibling;
        }
        l.match(')');
        // setup a return variable
        ScriptVarLink returnVar = null;
        // execute function!
        // add the function's execute space to the symbol table so we can recurse
        ScriptVarLink returnVarLink = functionRoot.addChild(TINYJS_RETURN_VAR);
        scopes.add(functionRoot);
    if(TINYJS_CALL_STACK)
        call_stack.add(function.name + " from " + l.getPosition());
    

        if (function.var.isNative()) {
            //ASSERT(function.var.jsCallback);
            try {
				function.var.jsCallback.invoke(functionRoot, function.var.jsCallbackUserData);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        } else {
            /* we just want to execute the block, but something could
             * have messed up and left us with the wrong ScriptLex, so
             * we want to be careful here... */
            Exception exception = null;
            ScriptLex oldLex = l;
            ScriptLex newLex = new ScriptLex(function.var.getString());
            l = newLex;
            try {
              block(execute);
              // because return will probably have called this, and set execute to false
              execute = true;
            } catch (Exception e) {
              exception = e;
            }
            
            // do some stuff, then throw the error; but really don't
            newLex.finalize();
            l = oldLex;

            // if (!Objects.isNull(exception)) throw exception;
        }
    if(TINYJS_CALL_STACK)
        if (!call_stack.isEmpty()) //call_stack.pop_back();
        	call_stack.remove(call_stack.size());
        //scopes.pop_back();
    	scopes.remove(scopes.size());
        /* get the real return var before we remove it from our function */
        returnVar = new ScriptVarLink(returnVarLink.var);
        functionRoot.removeLink(returnVarLink);
        functionRoot.finalize();
        if (!Objects.isNull(returnVar))
          return returnVar;
        else
          return new ScriptVarLink(new ScriptVar());
      } else {
        // function, but not executing - just parse args and be done
        l.match('(');
        while (l.tk != ')') {
          ScriptVarLink value = base(execute);
          CLEAN(value);
          if (l.tk!=')') l.match(',');
        }
        l.match(')');
        if (l.tk == '{') { // TODO: why is this here?
          block(execute);
        }
        /* function will be a blank scriptvarlink if we're not executing,
         * so just return it rather than an alloc/free */
        return function;
      }
    }

    ScriptVarLink factor(boolean execute) {
        if (l.tk=='(') {
            l.match('(');
            ScriptVarLink a = base(execute);
            l.match(')');
            return a;
        }
        if (l.tk==LexTypes.LEX_R_TRUE) {
            l.match(LexTypes.LEX_R_TRUE);
            return new ScriptVarLink(new ScriptVar(1));
        }
        if (l.tk==LexTypes.LEX_R_FALSE) {
            l.match(LexTypes.LEX_R_FALSE);
            return new ScriptVarLink(new ScriptVar(0));
        }
        if (l.tk==LexTypes.LEX_R_NULL) {
            l.match(LexTypes.LEX_R_NULL);
            return new ScriptVarLink(new ScriptVar(TINYJS_BLANK_DATA,ScriptVarFlags.SCRIPTVAR_NULL));
        }
        if (l.tk==LexTypes.LEX_R_UNDEFINED) {
            l.match(LexTypes.LEX_R_UNDEFINED);
            return new ScriptVarLink(new ScriptVar(TINYJS_BLANK_DATA,ScriptVarFlags.SCRIPTVAR_UNDEFINED));
        }
        if (l.tk==LexTypes.LEX_ID) {
            ScriptVarLink a = execute ? findInScopes(l.tkStr) : new ScriptVarLink(new ScriptVar());
            //printf("0x%08X for %s at %s\n", (unsigned int)a, l.tkStr.c_str(), l.getPosition().c_str());
            /* The parent if we're executing a method call */
            ScriptVar parent = null;

            if (execute && Objects.isNull(a)) {
              /* Variable doesn't exist! JavaScript says we should create it
               * (we won't add it here. This is done in the assignment operator)*/
              a = new ScriptVarLink(new ScriptVar(), l.tkStr);
            }
            l.match(LexTypes.LEX_ID);
            while (l.tk=='(' || l.tk=='.' || l.tk=='[') {
                if (l.tk=='(') { // ------------------------------------- Function Call
                    try {
						a = functionCall(execute, a, parent);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						;//e.printStackTrace();
					}
                } else if (l.tk == '.') { // ------------------------------------- Record Access
                    l.match('.');
                    if (execute) {
                      final String name = l.tkStr;
                      ScriptVarLink child = a.var.findChild(name);
                      if (Objects.isNull(child)) child = findInParentClasses(a.var, name);
                      if (Objects.isNull(child)) {
                        /* if we haven't found this defined yet, use the built-in
                           'length' properly */
                        if (a.var.isArray() && name == "length") {
                          int l = a.var.getArrayLength();
                          child = new ScriptVarLink(new ScriptVar(l));
                        } else if (a.var.isString() && name == "length") {
                          int l = a.var.getString().length();
                          child = new ScriptVarLink(new ScriptVar(l));
                        } else {
                          child = a.var.addChild(name);
                        }
                      }
                      parent = a.var;
                      a = child;
                    }
                    l.match(LexTypes.LEX_ID);
                } else if (l.tk == '[') { // ------------------------------------- Array Access
                    l.match('[');
                    ScriptVarLink index = base(execute);
                    l.match(']');
                    if (execute) {
                      ScriptVarLink child = a.var.findChildOrCreate(index.var.getString());
                      parent = a.var;
                      a = child;
                    }
                    CLEAN(index);
                } // else ASSERT(0);
            }
            return a;
        }
        if (l.tk==LexTypes.LEX_INT || l.tk==LexTypes.LEX_FLOAT) {
            ScriptVar a = new ScriptVar(l.tkStr,
                ((l.tk==LexTypes.LEX_INT)?ScriptVarFlags.SCRIPTVAR_INTEGER:ScriptVarFlags.SCRIPTVAR_DOUBLE));
            l.match(l.tk);
            return new ScriptVarLink(a);
        }
        if (l.tk==LexTypes.LEX_STR) {
            ScriptVar a = new ScriptVar(l.tkStr, ScriptVarFlags.SCRIPTVAR_STRING);
            l.match(LexTypes.LEX_STR);
            return new ScriptVarLink(a);
        }
        if (l.tk=='{') {
            ScriptVar contents = new ScriptVar(TINYJS_BLANK_DATA, ScriptVarFlags.SCRIPTVAR_OBJECT);
            /* JSON-style object definition */
            l.match('{');
            while (l.tk != '}') {
              String id = l.tkStr;
              // we only allow strings or IDs on the left hand side of an initialisation
              if (l.tk==LexTypes.LEX_STR) l.match(LexTypes.LEX_STR);
              else l.match(LexTypes.LEX_ID);
              l.match(':');
              if (execute) {
                ScriptVarLink a = base(execute);
                contents.addChild(id, a.var);
                CLEAN(a);
              }
              // no need to clean here, as it will definitely be used
              if (l.tk != '}') l.match(',');
            }

            l.match('}');
            return new ScriptVarLink(contents);
        }
        if (l.tk=='[') {
            ScriptVar contents = new ScriptVar(TINYJS_BLANK_DATA, ScriptVarFlags.SCRIPTVAR_ARRAY);
            /* JSON-style array */
            l.match('[');
            int idx = 0;
            while (l.tk != ']') {
              if (execute) {
                ScriptVarLink a = base(execute);
                contents.addChild(String.valueOf(idx), a.var);
                CLEAN(a);
              }
              // no need to clean here, as it will definitely be used
              if (l.tk != ']') l.match(',');
              idx++;
            }
            l.match(']');
            return new ScriptVarLink(contents);
        }
        if (l.tk==LexTypes.LEX_R_FUNCTION) {
          ScriptVarLink funcVar = parseFunctionDefinition();
            // if (funcVar.name != TINYJS_TEMP_NAME) TRACE("Functions not defined at statement-level are not meant to have a name");
            return funcVar;
        }
        if (l.tk==LexTypes.LEX_R_NEW) {
          // new . create a new object
          l.match(LexTypes.LEX_R_NEW);
          final String className = l.tkStr;
          if (execute) {
            ScriptVarLink objClassOrFunc = findInScopes(className);
            if (Objects.isNull(objClassOrFunc)) {
              //TRACE("%s is not a valid class name", className);
              return new ScriptVarLink(new ScriptVar());
            }
            l.match(LexTypes.LEX_ID);
            ScriptVar obj = new ScriptVar(TINYJS_BLANK_DATA, ScriptVarFlags.SCRIPTVAR_OBJECT);
            ScriptVarLink objLink = new ScriptVarLink(obj);
            if (objClassOrFunc.var.isFunction()) {
              try {
				CLEAN(functionCall(execute, objClassOrFunc, obj));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            } else {
              obj.addChild(TINYJS_PROTOTYPE_CLASS, objClassOrFunc.var);
              if (l.tk == '(') {
                l.match('(');
                l.match(')');
              }
            }
            return objLink;
          } else {
            l.match(LexTypes.LEX_ID);
            if (l.tk == '(') {
              l.match('(');
              l.match(')');
            }
          }
        }
        // Nothing we can do here... just hope it's the end...
        l.match(LexTypes.LEX_EOF);
        return null;
    }

    ScriptVarLink unary(boolean execute) {
        ScriptVarLink a;
        if (l.tk=='!') {
            l.match('!'); // binary not
            a = factor(execute);
            if (execute) {
				try {
					ScriptVar res = a.var.mathsOp(new ScriptVar(0), LexTypes.LEX_EQUAL);
					CREATE_LINK(a, res);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}   
            }
        } else
            a = factor(execute);
        return a;
    }

    ScriptVarLink term(boolean execute) {
        ScriptVarLink a = unary(execute);
        while (l.tk=='*' || l.tk=='/' || l.tk=='%') {
            int op = l.tk;
            l.match(l.tk);
            ScriptVarLink b = unary(execute);
            if (execute) {
				try {
					ScriptVar res = a.var.mathsOp(b.var, op);
					CREATE_LINK(a, res);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}    
            }
            CLEAN(b);
        }
        return a;
    }

    ScriptVarLink expression(boolean execute) {
        boolean negate = false;
        if (l.tk=='-') {
            l.match('-');
            negate = true;
        }
        ScriptVarLink a = term(execute);
        if (negate) {
			try {
				ScriptVar zero = new ScriptVar(0);
	            ScriptVar res = zero.mathsOp(a.var, '-');
				CREATE_LINK(a, res);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
        }

        while (l.tk=='+' ||
        		l.tk=='-' ||
        		l.tk==LexTypes.LEX_PLUSPLUS ||
            	l.tk==LexTypes.LEX_MINUSMINUS) {
            int op = l.tk;
            l.match(l.tk);
            if (op==LexTypes.LEX_PLUSPLUS || op==LexTypes.LEX_MINUSMINUS) {
            	if (execute) {
					try {
						ScriptVar res = a.var.mathsOp(new ScriptVar(1), op==LexTypes.LEX_PLUSPLUS ? '+' : '-');
	                    ScriptVarLink oldValue = new ScriptVarLink(a.var);
	                    // in-place add/subtract
	                    a.replaceWith(res);
	                    CLEAN(a);
	                    a = oldValue;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
            } else {
                ScriptVarLink b = term(execute);
                if (execute) {
                    // not in-place, so just replace
					try {
						ScriptVar res = a.var.mathsOp(b.var, op);
						CREATE_LINK(a, res);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}  
                }
                CLEAN(b);
            }
        }
        return a;
    }

    ScriptVarLink shift(boolean execute) {
      ScriptVarLink a = expression(execute);
      if (l.tk==LexTypes.LEX_LSHIFT || l.tk==LexTypes.LEX_RSHIFT || l.tk==LexTypes.LEX_RSHIFTUNSIGNED) {
        int op = l.tk;
        l.match(op);
        ScriptVarLink b = base(execute);
        int shift = execute ? b.var.getInt() : 0;
        CLEAN(b);
        if (execute) {
          if (op==LexTypes.LEX_LSHIFT) a.var.setInt(a.var.getInt() << shift);
          if (op==LexTypes.LEX_RSHIFT) a.var.setInt(a.var.getInt() >> shift);
          if (op==LexTypes.LEX_RSHIFTUNSIGNED) a.var.setInt(a.var.getInt() >> shift);
        }
      }
      return a;
    }

    ScriptVarLink condition(boolean execute) {
        ScriptVarLink a = shift(execute);
        ScriptVarLink b;
        while (l.tk==LexTypes.LEX_EQUAL || l.tk==LexTypes.LEX_NEQUAL ||
               l.tk==LexTypes.LEX_TYPEEQUAL || l.tk==LexTypes.LEX_NTYPEEQUAL ||
               l.tk==LexTypes.LEX_LEQUAL || l.tk==LexTypes.LEX_GEQUAL ||
               l.tk=='<' || l.tk=='>') {
            int op = l.tk;
            l.match(l.tk);
            b = shift(execute);
            if (execute) {
                try {
					ScriptVar res = a.var.mathsOp(b.var, op);
					CREATE_LINK(a,res);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                
            }
            CLEAN(b);
        }
        return a;
    }

    ScriptVarLink logic(boolean execute) {
        ScriptVarLink a = condition(execute);
        ScriptVarLink b;
        while (l.tk=='&' || l.tk=='|' || l.tk=='^' || l.tk==LexTypes.LEX_ANDAND || l.tk==LexTypes.LEX_OROR) {
            boolean noexecute = false;
            int op = l.tk;
            l.match(l.tk);
            boolean shortCircuit = false;
            boolean bool = false;
            // if we have short-circuit ops, then if we know the outcome
            // we don't bother to execute the other op. Even if not
            // we need to tell mathsOp it's an & or |
            if (op==LexTypes.LEX_ANDAND) {
                op = '&';
                shortCircuit = !a.var.getBool();
                bool = true;
            } else if (op==LexTypes.LEX_OROR) {
                op = '|';
                shortCircuit = a.var.getBool();
                bool = true;
            }
            b = condition(shortCircuit ? noexecute : execute);
            if (execute && !shortCircuit) {
                if (bool) {
                  ScriptVar newa = new ScriptVar(a.var.getBool()?1:0);
                  ScriptVar newb = new ScriptVar(b.var.getBool()?1:0);
                  CREATE_LINK(a, newa);
                  CREATE_LINK(b, newb);
                }
                try {
					ScriptVar res = a.var.mathsOp(b.var, op);
					CREATE_LINK(a, res);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}   
            }
            CLEAN(b);
        }
        return a;
    }

    ScriptVarLink ternary(boolean execute) {
      ScriptVarLink lhs = logic(execute);
      boolean noexec = false;
      if (l.tk=='?') {
        l.match('?');
        if (!execute) {
          CLEAN(lhs);
          CLEAN(base(noexec));
          l.match(':');
          CLEAN(base(noexec));
        } else {
          boolean first = lhs.var.getBool();
          CLEAN(lhs);
          if (first) {
            lhs = base(execute);
            l.match(':');
            CLEAN(base(noexec));
          } else {
            CLEAN(base(noexec));
            l.match(':');
            lhs = base(execute);
          }
        }
      }

      return lhs;
    }

    ScriptVarLink base(boolean execute) {
        ScriptVarLink lhs = ternary(execute);
        if (l.tk=='=' || l.tk==LexTypes.LEX_PLUSEQUAL || l.tk==LexTypes.LEX_MINUSEQUAL) {
            /* If we're assigning to this and we don't have a parent,
             * add it to the symbol table root as per JavaScript. */
            if (execute && !lhs.owned) {
              if (lhs.name.length()>0) {
                ScriptVarLink realLhs = root.addChildNoDup(lhs.name, lhs.var);
                CLEAN(lhs);
                lhs = realLhs;
              } // else TRACE("Trying to assign to an un-named type\n");
            }

            int op = l.tk;
            l.match(l.tk);
            ScriptVarLink rhs = base(execute);
            if (execute) {
            	try {
                if (op=='=') {
                    lhs.replaceWith(rhs);
                } else if (op==LexTypes.LEX_PLUSEQUAL) {
                    ScriptVar res = lhs.var.mathsOp(rhs.var, '+');
                    lhs.replaceWith(res);
                } else if (op==LexTypes.LEX_MINUSEQUAL) {
                    ScriptVar res = lhs.var.mathsOp(rhs.var, '-');
                    lhs.replaceWith(res);
                } //else ASSERT(0);
            	} catch(Exception e) {
            		;
            	}
            }
            CLEAN(rhs);
        }
        return lhs;
    }

    void block(boolean execute) {
        l.match('{');
        if (execute) {
          while (!Objects.isNull(l.tk) && l.tk!='}')
            statement(execute);
          l.match('}');
        } else {
          // fast skip of blocks
          int brackets = 1;
          while (!Objects.isNull(l.tk) && brackets>0) {
            if (l.tk == '{') brackets++;
            if (l.tk == '}') brackets--;
            l.match(l.tk);
          }
        }

    }

    void statement(boolean execute) {
        if (l.tk==LexTypes.LEX_ID ||
            l.tk==LexTypes.LEX_INT ||
            l.tk==LexTypes.LEX_FLOAT ||
            l.tk==LexTypes.LEX_STR ||
            l.tk=='-') {
            /* Execute a simple statement that only contains basic arithmetic... */
            CLEAN(base(execute));
            l.match(';');
        } else if (l.tk=='{') {
            /* A block of code */
            block(execute);
        } else if (l.tk==';') {
            /* Empty statement - to allow things like ;;; */
            l.match(';');
        } else if (l.tk==LexTypes.LEX_R_VAR) {
            /* variable creation. TODO - we need a better way of parsing the left
             * hand side. Maybe just have a flag called can_create_var that we
             * set and then we parse as if we're doing a normal equals.*/
            l.match(LexTypes.LEX_R_VAR);
            while (l.tk != ';') {
              ScriptVarLink a = null;
              if (execute)
                //a = scopes.back().findChildOrCreate(l.tkStr);
            	  a = scopes.get(scopes.size()).findChildOrCreate(l.tkStr);
              l.match(LexTypes.LEX_ID);
              // now do stuff defined with dots
              while (l.tk == '.') {
                  l.match('.');
                  if (execute) {
                      ScriptVarLink lastA = a;
                      a = lastA.var.findChildOrCreate(l.tkStr);
                  }
                  l.match(LexTypes.LEX_ID);
              }
              // sort out initialiser
              if (l.tk == '=') {
                  l.match('=');
                  ScriptVarLink var = base(execute);
                  if (execute)
                      a.replaceWith(var);
                  CLEAN(var);
              }
              if (l.tk != ';')
                l.match(',');
            }       
            l.match(';');
        } else if (l.tk==LexTypes.LEX_R_IF) {
            l.match(LexTypes.LEX_R_IF);
            l.match('(');
            ScriptVarLink var = base(execute);
            l.match(')');
            boolean cond = execute && var.var.getBool();
            CLEAN(var);
            boolean noexecute = false; // because we need to be abl;e to write to it
            statement(cond ? execute : noexecute);
            if (l.tk==LexTypes.LEX_R_ELSE) {
                l.match(LexTypes.LEX_R_ELSE);
                statement(cond ? noexecute : execute);
            }
        } else if (l.tk==LexTypes.LEX_R_WHILE) {
            // We do repetition by pulling out the string representing our statement
            // there's definitely some opportunity for optimisation here
            l.match(LexTypes.LEX_R_WHILE);
            l.match('(');
            int whileCondStart = l.tokenStart;
            boolean noexecute = false;
            ScriptVarLink cond = base(execute);
            boolean loopCond = execute && cond.var.getBool();
            CLEAN(cond);
            ScriptLex whileCond = l.getSubLex(whileCondStart);
            l.match(')');
            int whileBodyStart = l.tokenStart;
            statement(loopCond ? execute : noexecute);
            ScriptLex whileBody = l.getSubLex(whileBodyStart);
            ScriptLex oldLex = l;
            int loopCount = TINYJS_LOOP_MAX_ITERATIONS;
            while (loopCond && loopCount > 0) {
                whileCond.reset();
                l = whileCond;
                cond = base(execute);
                loopCond = execute && cond.var.getBool();
                CLEAN(cond);
                if (loopCond) {
                    whileBody.reset();
                    l = whileBody;
                    statement(execute);
                }
            }
            l = oldLex;
            whileCond.finalize();
            whileBody.finalize();
            /*
            if (loopCount<=0) {
                root.trace();
                TRACE("WHILE Loop exceeded %d iterations at %s\n", TINYJS_LOOP_MAX_ITERATIONS, l.getPosition().c_str());
                throw new CScriptException("LOOP_ERROR");
            }
            */
        } else if (l.tk==LexTypes.LEX_R_FOR) {
            l.match(LexTypes.LEX_R_FOR);
            l.match('(');
            statement(execute); // initialisation
            //l.match(';');
            int forCondStart = l.tokenStart;
            boolean noexecute = false;
            ScriptVarLink cond = base(execute); // condition
            boolean loopCond = execute && cond.var.getBool();
            CLEAN(cond);
            ScriptLex forCond = l.getSubLex(forCondStart);
            l.match(';');
            int forIterStart = l.tokenStart;
            CLEAN(base(noexecute)); // iterator
            ScriptLex forIter = l.getSubLex(forIterStart);
            l.match(')');
            int forBodyStart = l.tokenStart;
            statement(loopCond ? execute : noexecute);
            ScriptLex forBody = l.getSubLex(forBodyStart);
            ScriptLex oldLex = l;
            if (loopCond) {
                forIter.reset();
                l = forIter;
                CLEAN(base(execute));
            }
            int loopCount = TINYJS_LOOP_MAX_ITERATIONS;
            while (execute && loopCond && loopCount > 0) {
                forCond.reset();
                l = forCond;
                cond = base(execute);
                loopCond = cond.var.getBool();
                CLEAN(cond);
                if (execute && loopCond) {
                    forBody.reset();
                    l = forBody;
                    statement(execute);
                }
                if (execute && loopCond) {
                    forIter.reset();
                    l = forIter;
                    CLEAN(base(execute));
                }
            }
            l = oldLex;
            forCond.finalize();
            forIter.finalize();
            forBody.finalize();
            /*
            if (loopCount<=0) {
                root.trace();
                TRACE("FOR Loop exceeded %d iterations at %s\n", TINYJS_LOOP_MAX_ITERATIONS, l.getPosition().c_str());
                throw new CScriptException("LOOP_ERROR");
            }
            */
        } else if (l.tk==LexTypes.LEX_R_RETURN) {
            l.match(LexTypes.LEX_R_RETURN);
            ScriptVarLink result = null;
            if (l.tk != ';')
              result = base(execute);
            if (execute) {
              ScriptVarLink resultVar = scopes.get(scopes.size()).findChild(TINYJS_RETURN_VAR);
              if (!Objects.isNull(resultVar))
                resultVar.replaceWith(result);
              // else TRACE("RETURN statement, but not in a function.\n");
              execute = false;
            }
            CLEAN(result);
            l.match(';');
        } else if (l.tk==LexTypes.LEX_R_FUNCTION) {
            ScriptVarLink funcVar = parseFunctionDefinition();
            if (execute) {
              if (funcVar.name != TINYJS_TEMP_NAME)
                //scopes.back().addChildNoDup(funcVar.name, funcVar.var);
            	  scopes.get(scopes.size()).addChildNoDup(funcVar.name, funcVar.var);
              // else TRACE("Functions defined at statement-level are meant to have a name\n");
            }
            CLEAN(funcVar);
        } else l.match(LexTypes.LEX_EOF);
    }

    /// Get the given variable specified by a path (var1.var2.etc), or return 0
    ScriptVar getScriptVariable(final String path) {
        // traverse path
        int prevIdx = 0;
        int thisIdx = path.indexOf('.');
        if (thisIdx == (-1)) thisIdx = path.length();
        ScriptVar var = root;
        while (!Objects.isNull(var) && prevIdx<path.length()) {
            String el = path.substring(prevIdx, thisIdx-prevIdx);
            ScriptVarLink varl = var.findChild(el);
            var = !Objects.isNull(varl)?varl.var:null;
            prevIdx = thisIdx+1;
            thisIdx = path.indexOf('.', prevIdx);
            if (thisIdx == (-1)) thisIdx = path.length();
        }
        return var;
    }

    /// Get the value of the given variable, or return 0
    final String getVariable(final String path) {
        ScriptVar var = getScriptVariable(path);
        // return result
        if (!Objects.isNull(var))
            return var.getString();
        else
            return "";
    }

    /// set the value of the given variable, return trur if it exists and gets set
    boolean setVariable(final String path, final String varData) {
        ScriptVar var = getScriptVariable(path);
        // return result
        if (!Objects.isNull(var)) {
            if (var.isInt())
                var.setInt(Integer.parseInt(varData));
            else if (var.isDouble())
                var.setDouble(Double.parseDouble(varData));
            else
                var.setString(varData);
            return true;
        }    
        else
            return false;
    }

    /// Finds a child, looking recursively up the scopes
    ScriptVarLink findInScopes(final String childName) {
        for (int s=scopes.size()-1;s>=0;s--) {
          ScriptVarLink v = scopes.get(s).findChild(childName);
          if (!Objects.isNull(v)) return v;
        }
        return null;

    }

    /// Look up in any parent classes of the given object
    ScriptVarLink findInParentClasses(ScriptVar object, final String name) {
        // Look for links to actual parent classes
        ScriptVarLink parentClass = object.findChild(TINYJS_PROTOTYPE_CLASS);
        while (!Objects.isNull(parentClass)) {
          ScriptVarLink implementation = parentClass.var.findChild(name);
          if (!Objects.isNull(implementation)) return implementation;
          parentClass = parentClass.var.findChild(TINYJS_PROTOTYPE_CLASS);
        }
        // else fake it for strings and finally objects
        if (object.isString()) {
          ScriptVarLink implementation = stringClass.findChild(name);
          if (!Objects.isNull(implementation)) return implementation;
        }
        if (object.isArray()) {
          ScriptVarLink implementation = arrayClass.findChild(name);
          if (!Objects.isNull(implementation)) return implementation;
        }
        ScriptVarLink implementation = objectClass.findChild(name);
        if (!Objects.isNull(implementation)) return implementation;

        return null;
    }

	    
}
