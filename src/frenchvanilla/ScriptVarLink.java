package frenchvanilla;

import java.util.Objects;

public class ScriptVarLink {
	
	public String name;
	public ScriptVarLink nextSibling;
	public ScriptVarLink prevSibling;
	public ScriptVar var;
	public boolean owned;	

	
	public ScriptVarLink(ScriptVar var) {
	    this.name = TinyJS.TINYJS_TEMP_NAME;
	    this.nextSibling = null;
	    this.prevSibling = null;
	    this.var = var.ref();
	    this.owned = false;
	}
		
	public ScriptVarLink(ScriptVar var, final String name) {
	    this.name = name;
	    this.nextSibling = null;
	    this.prevSibling = null;
	    this.var = var.ref();
	    this.owned = false;
	}
	
	///< Copy constructor
	public ScriptVarLink(final ScriptVarLink link) {
	    this.name = link.name;
	    this.nextSibling = null;
	    this.prevSibling = null;
	    this.var = link.var.ref();
	    this.owned = false;
	}
	
	protected void finalize() {
	    var.unref();
	}
	
	///< Replace the Variable pointed to
	void replaceWith(ScriptVar newVar) {
	    ScriptVar oldVar = var;
	    var = newVar.ref();
	    oldVar.unref();
	}
	
	///< Replace the Variable pointed to (just dereferences)
	void replaceWith(ScriptVarLink newVar) {
	    if (!Objects.isNull(newVar))
	      replaceWith(newVar.var);
	    else
	      replaceWith(new ScriptVar());
	}
	
	///< Get the name as an integer (for arrays)
	public int getIntName() {
	    return Utils.atoi(name);
	}
	
	///< Set the name as an integer (for arrays)
	public void setIntName(int n) {
	    //sprintf_s(64-byte-string, 64, "%d", n);
	    name = String.valueOf(n);
	}

} // class
