package frenchvanilla;

public class LexTypes {

	public static final int LEX_EOF = 0;
	
	public static final int LEX_INT = 1;
	public static final int LEX_FLOAT = 2;
	public static final int LEX_STR = 3;

	public static final int LEX_EQUAL = 4;
	public static final int LEX_TYPEEQUAL = 5;
    public static final int LEX_NEQUAL = 6;
    public static final int LEX_NTYPEEQUAL = 7;
    public static final int LEX_LEQUAL = 8;
    public static final int LEX_LSHIFT = 9;
    public static final int LEX_LSHIFTEQUAL = 10;
    public static final int LEX_GEQUAL = 11;
    public static final int LEX_RSHIFT = 12;
    public static final int LEX_RSHIFTUNSIGNED = 13;
    public static final int LEX_RSHIFTEQUAL = 14;
    public static final int LEX_PLUSEQUAL = 15;
    public static final int LEX_MINUSEQUAL = 16;
    public static final int LEX_PLUSPLUS = 17;
    public static final int LEX_MINUSMINUS = 18;
    public static final int LEX_ANDEQUAL = 19;
    public static final int LEX_ANDAND = 20;
    public static final int LEX_OREQUAL = 21;
    public static final int LEX_OROR = 22;
    public static final int LEX_XOREQUAL = 23;
    
    public static final int LEX_ID = 256;
	
    // reserved words
    public static final int LEX_R_LIST_START = 24;
    
    public static final int LEX_R_IF = 25;
    public static final int LEX_R_ELSE = 26;
    public static final int LEX_R_DO = 27;
    public static final int LEX_R_WHILE = 28;
    public static final int LEX_R_FOR = 29;
    public static final int LEX_R_BREAK = 30;
    public static final int LEX_R_CONTINUE = 31;
    public static final int LEX_R_FUNCTION = 32;
    public static final int LEX_R_RETURN = 33;
    public static final int LEX_R_VAR = 34;
    public static final int LEX_R_TRUE = 35;
    public static final int LEX_R_FALSE = 36;
    public static final int LEX_R_NULL = 37;
    public static final int LEX_R_UNDEFINED = 38;
    public static final int LEX_R_NEW = 39;

    public static final int LEX_R_LIST_END = 40;
	
}
