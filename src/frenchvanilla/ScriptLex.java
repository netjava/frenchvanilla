package frenchvanilla;


public class ScriptLex {

    char currCh, nextCh;
    int tk; ///< The type of the token that we have
    int tokenStart; ///< Position in the data at the beginning of the token we have here
    int tokenEnd; ///< Position in the data at the last character of the token we have here
    int tokenLastEnd; ///< Position in the data at the last character of the last token
    String tkStr; ///< Data contained in the token we have here


    /* When we go into a loop, we use getSubLex to get a lexer for just the sub-part of the
       relevant string. This doesn't re-allocate and copy the string, but instead copies
       the data pointer and sets dataOwned to false, and dataStart/dataEnd to the relevant things. */
    protected char[] data; ///< Data string to get tokens from
    protected int dataStart, dataEnd; ///< Start and end position in data string
    protected boolean dataOwned; ///< Do we own this data string?

    protected int dataPos; ///< Position in data (we CAN go past the end of the string here)

 
    

ScriptLex(final String input) {
    data = input.toCharArray();
    dataOwned = true;
    dataStart = 0;
    dataEnd = data.length;
    reset();
}

ScriptLex(ScriptLex owner, int startChar, int endChar) {
    data = owner.data;
    dataOwned = false;
    dataStart = startChar;
    dataEnd = endChar;
    reset();
}

protected void finalize()
{
	//erase data
    if (dataOwned)
        data=null;
}

///< Reset this lex so we can start again
public void reset() {
    dataPos = dataStart;
    tokenStart = 0;
    tokenEnd = 0;
    tokenLastEnd = 0;
    tk = 0;
    tkStr = "";
    getNextCh();
    getNextCh();
    getNextToken();
}

///< Lexical match wotsit
public void match(int expected_tk) {
	try {
    if (tk!=expected_tk)
        throw new Exception("Got " + getTokenStr(tk) + " expected " + getTokenStr(expected_tk) + " at " + getPosition(tokenStart));
    getNextToken();
	} catch (Exception e) {
		; // do nothing
	}
}

///< Get the string representation of the given token
public static String getTokenStr(int token) {
    if (token>32 && token<128) {
        char buf[] = {'"','\'','\'', '"'};
        buf[1] = (char)token;
        return buf.toString();
    }
    switch (token) {
    case LexTypes.LEX_EOF : return "EOF";
    case LexTypes.LEX_ID : return "ID";
    case LexTypes.LEX_INT : return "INT";
    case LexTypes.LEX_FLOAT : return "FLOAT";
    case LexTypes.LEX_STR : return "STRING";
    case LexTypes.LEX_EQUAL : return "==";
    case LexTypes.LEX_TYPEEQUAL : return "===";
    case LexTypes.LEX_NEQUAL : return "!=";
    case LexTypes.LEX_NTYPEEQUAL : return "!==";
    case LexTypes.LEX_LEQUAL : return "<=";
    case LexTypes.LEX_LSHIFT : return "<<";
    case LexTypes.LEX_LSHIFTEQUAL : return "<<=";
    case LexTypes.LEX_GEQUAL : return ">=";
    case LexTypes.LEX_RSHIFT : return ">>";
    // copies?
    case LexTypes.LEX_RSHIFTUNSIGNED : return ">>";
    case LexTypes.LEX_RSHIFTEQUAL : return ">>=";
    case LexTypes.LEX_PLUSEQUAL : return "+=";
    case LexTypes.LEX_MINUSEQUAL : return "-=";
    case LexTypes.LEX_PLUSPLUS : return "++";
    case LexTypes.LEX_MINUSMINUS : return "--";
    case LexTypes.LEX_ANDEQUAL : return "&=";
    case LexTypes.LEX_ANDAND : return "&&";
    case LexTypes.LEX_OREQUAL : return "|=";
    case LexTypes.LEX_OROR : return "||";
    case LexTypes.LEX_XOREQUAL : return "^=";
            // reserved words
    case LexTypes.LEX_R_IF : return "if";
    case LexTypes.LEX_R_ELSE : return "else";
    case LexTypes.LEX_R_DO : return "do";
    case LexTypes.LEX_R_WHILE : return "while";
    case LexTypes.LEX_R_FOR : return "for";
    case LexTypes.LEX_R_BREAK : return "break";
    case LexTypes.LEX_R_CONTINUE : return "continue";
    case LexTypes.LEX_R_FUNCTION : return "function";
    case LexTypes.LEX_R_RETURN : return "return";
    case LexTypes.LEX_R_VAR : return "var";
    case LexTypes.LEX_R_TRUE : return "true";
    case LexTypes.LEX_R_FALSE : return "false";
    case LexTypes.LEX_R_NULL : return "null";
    case LexTypes.LEX_R_UNDEFINED : return "undefined";
    case LexTypes.LEX_R_NEW : return "new";
}

    // unknown token
    return new String("?[" + token + "]");
}

public void getNextCh() {
    currCh = nextCh;
    if (dataPos < dataEnd)
        nextCh = data[dataPos];
    else
        nextCh = 0;
    dataPos++;
}


///< Get the text token from our text string
public void getNextToken() {
    tk = LexTypes.LEX_EOF;
    tkStr="";
    while (currCh > 0 && Utils.isWhitespace(currCh)) getNextCh();
    // newline comments
    if (currCh=='/' && nextCh=='/') {
        while (currCh > 0 && currCh!='\n') getNextCh();
        getNextCh();
        getNextToken();
        return;
    }
    // block comments
    if (currCh == '/' && nextCh == '*') {
        while (currCh > 0 && (currCh!='*' || nextCh!='/')) getNextCh();
        getNextCh();
        getNextCh();
        getNextToken();
        return;
    }
    // record beginning of this token
    tokenStart = dataPos-2;
    // tokens
    if (Utils.isAlpha(currCh)) { //  IDs
        while (Utils.isAlpha(currCh) || Utils.isNumeric(currCh)) {
            tkStr += currCh;
            getNextCh();
        }
        tk = LexTypes.LEX_ID;
             if (tkStr=="if") tk = LexTypes.LEX_R_IF;
        else if (tkStr=="else") tk = LexTypes.LEX_R_ELSE;
        else if (tkStr=="do") tk = LexTypes.LEX_R_DO;
        else if (tkStr=="while") tk = LexTypes.LEX_R_WHILE;
        else if (tkStr=="for") tk = LexTypes.LEX_R_FOR;
        else if (tkStr=="break") tk = LexTypes.LEX_R_BREAK;
        else if (tkStr=="continue") tk = LexTypes.LEX_R_CONTINUE;
        else if (tkStr=="function") tk = LexTypes.LEX_R_FUNCTION;
        else if (tkStr=="return") tk = LexTypes.LEX_R_RETURN;
        else if (tkStr=="var") tk = LexTypes.LEX_R_VAR;
        else if (tkStr=="true") tk = LexTypes.LEX_R_TRUE;
        else if (tkStr=="false") tk = LexTypes.LEX_R_FALSE;
        else if (tkStr=="null") tk = LexTypes.LEX_R_NULL;
        else if (tkStr=="undefined") tk = LexTypes.LEX_R_UNDEFINED;
        else if (tkStr=="new") tk = LexTypes.LEX_R_NEW;
    } else if (Utils.isNumeric(currCh)) { // Numbers
        boolean isHex = false;
        if (currCh=='0') { tkStr += currCh; getNextCh(); }
        if (currCh=='x') {
          isHex = true;
          tkStr += currCh; getNextCh();
        }
        tk = LexTypes.LEX_INT;
        while (Utils.isNumeric(currCh) || (isHex && Utils.isHexadecimal(currCh))) {
            tkStr += currCh;
            getNextCh();
        }
        if (!isHex && currCh=='.') {
            tk = LexTypes.LEX_FLOAT;
            tkStr += '.';
            getNextCh();
            while (Utils.isNumeric(currCh)) {
                tkStr += currCh;
                getNextCh();
            }
        }
        // do fancy e-style floating point
        if (!isHex && (currCh=='e'||currCh=='E')) {
          tk = LexTypes.LEX_FLOAT;
          tkStr += currCh; getNextCh();
          if (currCh=='-') { tkStr += currCh; getNextCh(); }
          while (Utils.isNumeric(currCh)) {
             tkStr += currCh; getNextCh();
          }
        }
    } else if (currCh=='"') {
        // strings...
        getNextCh();
        while (currCh > 0 && currCh!='"') {
            if (currCh == '\\') {
                getNextCh();
                switch (currCh) {
                case 'n' : tkStr += '\n'; break;
                case '"' : tkStr += '"'; break;
                case '\\' : tkStr += '\\'; break;
                default: tkStr += currCh;
                }
            } else {
                tkStr += currCh;
            }
            getNextCh();
        }
        getNextCh();
        tk = LexTypes.LEX_STR;
    } else if (currCh=='\'') {
        // strings again...
        getNextCh();
        while (currCh > 0 && currCh!='\'') {
            if (currCh == '\\') {
                getNextCh();
                switch (currCh) {
                case 'n' : tkStr += '\n'; break;
                case 'a' : tkStr += (char)9; break;
                case 'r' : tkStr += '\r'; break;
                case 't' : tkStr += '\t'; break;
                case '\'' : tkStr += '\''; break;
                case '\\' : tkStr += '\\'; break;
                //TODO:  expand this to actually 64 bit hex
                case 'x' : { // hex digits
                              char buf[] = {'?','?','\0'};
                              getNextCh(); buf[0] = currCh;
                              getNextCh(); buf[1] = currCh;
                              // convert the hex values to decimal
                              //tkStr += (char)strtol(buf,0,16);
                              tkStr += String.valueOf(Integer.parseInt(buf.toString(), 16));
                           } break;
                           // backslash-number?
                default: if (currCh>='0' && currCh<='7') {
                           // octal digits
                           char buf[] = {'?','?','?','\0'};
                           buf[0] = currCh;
                           getNextCh(); buf[1] = currCh;
                           getNextCh(); buf[2] = currCh;
                           //tkStr += (char)strtol(buf,0,8);
                           tkStr += String.valueOf(Long.parseLong(buf.toString(), 8));
                         } else
                           tkStr += currCh;
                }
            } else {
                tkStr += currCh;
            }
            getNextCh();
        }
        getNextCh();
        tk = LexTypes.LEX_STR;
    } else {
        // single chars
        tk = currCh;
        if (currCh>0) getNextCh();
        if (tk=='=' && currCh=='=') { // ==
            tk = LexTypes.LEX_EQUAL;
            getNextCh();
            if (currCh=='=') { // ===
              tk = LexTypes.LEX_TYPEEQUAL;
              getNextCh();
            }
        } else if (tk=='!' && currCh=='=') { // !=
            tk = LexTypes.LEX_NEQUAL;
            getNextCh();
            if (currCh=='=') { // !==
              tk = LexTypes.LEX_NTYPEEQUAL;
              getNextCh();
            }
        } else if (tk=='<' && currCh=='=') {
            tk = LexTypes.LEX_LEQUAL;
            getNextCh();
        } else if (tk=='<' && currCh=='<') {
            tk = LexTypes.LEX_LSHIFT;
            getNextCh();
            if (currCh=='=') { // <<=
              tk = LexTypes.LEX_LSHIFTEQUAL;
              getNextCh();
            }
        } else if (tk=='>' && currCh=='=') {
            tk = LexTypes.LEX_GEQUAL;
            getNextCh();
        } else if (tk=='>' && currCh=='>') {
            tk = LexTypes.LEX_RSHIFT;
            getNextCh();
            if (currCh=='=') { // >>=
              tk = LexTypes.LEX_RSHIFTEQUAL;
              getNextCh();
            } else if (currCh=='>') { // >>>
              tk = LexTypes.LEX_RSHIFTUNSIGNED;
              getNextCh();
            }
        }  else if (tk=='+' && currCh=='=') {
            tk = LexTypes.LEX_PLUSEQUAL;
            getNextCh();
        }  else if (tk=='-' && currCh=='=') {
            tk = LexTypes.LEX_MINUSEQUAL;
            getNextCh();
        }  else if (tk=='+' && currCh=='+') {
            tk = LexTypes.LEX_PLUSPLUS;
            getNextCh();
        }  else if (tk=='-' && currCh=='-') {
            tk = LexTypes.LEX_MINUSMINUS;
            getNextCh();
        } else if (tk=='&' && currCh=='=') {
            tk = LexTypes.LEX_ANDEQUAL;
            getNextCh();
        } else if (tk=='&' && currCh=='&') {
            tk = LexTypes.LEX_ANDAND;
            getNextCh();
        } else if (tk=='|' && currCh=='=') {
            tk = LexTypes.LEX_OREQUAL;
            getNextCh();
        } else if (tk=='|' && currCh=='|') {
            tk = LexTypes.LEX_OROR;
            getNextCh();
        } else if (tk=='^' && currCh=='=') {
            tk = LexTypes.LEX_XOREQUAL;
            getNextCh();
        }
    }
    /* This isn't quite right yet */
    tokenLastEnd = tokenEnd;
    tokenEnd = dataPos-3;
}

///< Return a sub-string from the given position up until right now
public String getSubString(int lastPosition) {
    int lastCharIdx = tokenLastEnd+1;
    if (lastCharIdx < dataEnd) {
        /* save a memory alloc by using our data array to create the
           substring */
        char old = data[lastCharIdx];
        data[lastCharIdx] = 0; 
        String value = new String(data).substring(lastPosition);
        data[lastCharIdx] = old;
        return value;
    } else {
        return new String(data).substring(lastPosition);
    }
}

///< Return a sub-lexer from the given position up until right now
public ScriptLex getSubLex(int lastPosition) {
    int lastCharIdx = tokenLastEnd+1;
    if (lastCharIdx < dataEnd)
        return new ScriptLex(this, lastPosition, lastCharIdx);
    else
        return new ScriptLex(this, lastPosition, dataEnd );
}

public String getPosition() {
	return getPosition(-1);
}

///< Return a string representing the position in lines and columns of the character pos given
public String getPosition(int pos) {
    if (pos<0) pos=tokenLastEnd;
    int line = 1,col = 1;
    for (int i=0;i<pos;i++) {
        char ch;
        if (i < dataEnd)
            ch = data[i];
        else
            ch = 0;
        col++;
        if (ch=='\n') {
            line++;
            col = 0;
        }
    }
    return new String("(line: "+line+", col: "+col+")");
}

} // class
