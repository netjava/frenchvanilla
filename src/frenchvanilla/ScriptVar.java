package frenchvanilla;

import java.lang.reflect.Method;
import java.util.Objects;

public class ScriptVar{
	
	public boolean getBool() { return (getInt() != 0); }
    public boolean isInt() { return (flags & ScriptVarFlags.SCRIPTVAR_INTEGER)!=0; }
    public boolean isDouble() { return (flags&ScriptVarFlags.SCRIPTVAR_DOUBLE)!=0; }
    public boolean isString() { return (flags&ScriptVarFlags.SCRIPTVAR_STRING)!=0; }
    public boolean isNumeric() { return (flags&ScriptVarFlags.SCRIPTVAR_NUMERICMASK)!=0; }
    public boolean isFunction() { return (flags&ScriptVarFlags.SCRIPTVAR_FUNCTION)!=0; }
    public boolean isObject() { return (flags&ScriptVarFlags.SCRIPTVAR_OBJECT)!=0; }
    public boolean isArray() { return (flags&ScriptVarFlags.SCRIPTVAR_ARRAY)!=0; }
    public boolean isNative() { return (flags&ScriptVarFlags.SCRIPTVAR_NATIVE)!=0; }
    public boolean isUndefined() { return (flags & ScriptVarFlags.SCRIPTVAR_VARTYPEMASK) == ScriptVarFlags.SCRIPTVAR_UNDEFINED; }
    public boolean isNull() { return (flags & ScriptVarFlags.SCRIPTVAR_NULL)!=0; }
    public boolean isBasic() { return (this.flags == ScriptVarFlags.SCRIPTVAR_INTEGER || this.flags == ScriptVarFlags.SCRIPTVAR_DOUBLE || this.flags == ScriptVarFlags.SCRIPTVAR_STRING)?true:false; } ///< Is this *not* an array/object/etc

    public ScriptVarLink firstChild;
    public ScriptVarLink lastChild;
        
    protected int refs; ///< The number of references held to this - used for garbage collection

    protected String strData; ///< The contents of this variable if it is a string
    protected int intData; ///< The contents of this variable if it is an int
    protected double doubleData; ///< The contents of this variable if it is a double
    
    protected int flags; ///< the flags determine the type of the variable - int/double/string/etc
    protected Method jsCallback; ///< Callback for native functions
    protected String jsCallbackUserData; ///< JSON user data passed as second argument to native functions

    //friend class CTinyJS;

public ScriptVar() {
    refs = 0;
    init();
    flags = ScriptVarFlags.SCRIPTVAR_UNDEFINED;
}

public ScriptVar(final String str) {
    refs = 0;
    init();
    flags = ScriptVarFlags.SCRIPTVAR_STRING;
    strData = str;
}

public ScriptVar(final String varData, int varFlags) {
    refs = 0;
    init();
    flags = varFlags;
    if ((varFlags & ScriptVarFlags.SCRIPTVAR_INTEGER)>0) {
      intData = Integer.parseInt(varData);
    } else if ((varFlags & ScriptVarFlags.SCRIPTVAR_DOUBLE) > 0) {
      doubleData = Double.parseDouble(varData);
    } else
      strData = varData;
}

public ScriptVar(double val) {
    refs = 0;
    init();
    // overwrite the initial value
    setDouble(val);
}

public ScriptVar(int val) {
    refs = 0;
    init();
    setInt(val);
}

protected void finalize() {
    removeAllChildren();
}

//should refs init also go here? where is init called, then old refs value used?
void init() {
    firstChild = null;
    lastChild = null;
    flags = 0;
    jsCallback = null;
    jsCallbackUserData = new String("");
    strData = TinyJS.TINYJS_BLANK_DATA;
    intData = 0;
    doubleData = 0D;
    // allocate a container so direct writes go somewhere
    strData = new String();
}

public ScriptVar getReturnVar() {
    return getParameter(TinyJS.TINYJS_RETURN_VAR);
}

public void setReturnVar(ScriptVar var) {
    findChildOrCreate(TinyJS.TINYJS_RETURN_VAR).replaceWith(var);
}


public ScriptVar getParameter(final String name) {
    return findChildOrCreate(name).var;
}

// linked-list scan search
ScriptVarLink findChild(final String childName) {
    ScriptVarLink v = firstChild;
    while (!Objects.isNull(v)) {
        if (v.name.equals(childName))
            return v;
        v = v.nextSibling;
    }
    return null;
}

ScriptVarLink findChildOrCreate(final String childName) {
    ScriptVarLink l = findChild(childName);
    if (!Objects.isNull(l)) return l;
    return addChild(childName, new ScriptVar(TinyJS.TINYJS_BLANK_DATA, ScriptVarFlags.SCRIPTVAR_UNDEFINED));
}

ScriptVarLink findChildOrCreate(final String childName, int varFlags ) {
    ScriptVarLink l = findChild(childName);
    if (!Objects.isNull(l)) return l;
    return addChild(childName, new ScriptVar(TinyJS.TINYJS_BLANK_DATA, varFlags));
}

ScriptVarLink findChildOrCreateByPath(final String path) {
  int p = path.indexOf('.');
	if (p == (-1))
    return findChildOrCreate(path);

  return findChildOrCreate(path.substring(0,p), ScriptVarFlags.SCRIPTVAR_OBJECT).var.
            findChildOrCreateByPath(path.substring(p+1));
}

public ScriptVarLink addChild(final String childName) {
	return addChild(childName, null);
}

public ScriptVarLink addChild(final String childName, ScriptVar child) {
  if (isUndefined()) {
    flags = ScriptVarFlags.SCRIPTVAR_OBJECT;
  }
    // if no child supplied, create one
    if (Objects.isNull(child))
      child = new ScriptVar();

    ScriptVarLink link = new ScriptVarLink(child, childName);
    link.owned = true;
    if (!Objects.isNull(lastChild)) {
        lastChild.nextSibling = link;
        link.prevSibling = lastChild;
        lastChild = link;
    } else {
        firstChild = link;
        lastChild = link;
    }
    return link;
}

public ScriptVarLink addChildNoDup(final String childName) {
	return addChildNoDup(childName, null);
}

public ScriptVarLink addChildNoDup(final String childName, ScriptVar child) {
    // if no child supplied, create one
    if (Objects.isNull(child))
      child = new ScriptVar();

    ScriptVarLink v = findChild(childName);
    if (!Objects.isNull(v)) {
        v.replaceWith(child);
    } else {
        v = addChild(childName, child);
    }

    return v;
}

void removeChild(ScriptVar child) {
    ScriptVarLink link = firstChild;
    while (!Objects.isNull(link)) {
        if (link.var.equals(child))
            break;
        link = link.nextSibling;
    }
    //ASSERT(link);
    removeLink(link);
}

void removeLink(ScriptVarLink link) {
    if (Objects.isNull(link)) return;
    if (!Objects.isNull(link.nextSibling))
      link.nextSibling.prevSibling = link.prevSibling;
    if (!Objects.isNull(link.prevSibling))
      link.prevSibling.nextSibling = link.nextSibling;
    if (lastChild.equals(link))
        lastChild = link.prevSibling;
    if (firstChild.equals(link))
        firstChild = link.nextSibling;
    link.finalize();
}

void removeAllChildren() {
    ScriptVarLink c = firstChild;
    while (!Objects.isNull(c)) {
        ScriptVarLink t = c.nextSibling;
        c.finalize();
        c = t;
    }
    firstChild = null;
    lastChild = null;
}

public ScriptVar getArrayIndex(int idx) {
    //sprintf_s(sIdx, sizeof(sIdx), "%d", idx);
    ScriptVarLink link = findChild(String.valueOf(idx));
    if (!Objects.isNull(link)) return link.var;
    else return new ScriptVar(TinyJS.TINYJS_BLANK_DATA, ScriptVarFlags.SCRIPTVAR_NULL); // undefined
}

public void setArrayIndex(int idx, ScriptVar value) {
    //sprintf_s(sIdx, sizeof(sIdx), "%d", idx);
    ScriptVarLink link = findChild(String.valueOf(idx));

    if (!Objects.isNull(link)) {
      if (value.isUndefined())
        removeLink(link);
      else
        link.replaceWith(value);
    } else {
      if (!value.isUndefined())
        addChild(String.valueOf(idx), value);
    }
}

public int getArrayLength() {
    int highest = -1;
    if (!isArray()) return 0;

    ScriptVarLink link = firstChild;
    while (!Objects.isNull(link)) {
      if (Utils.isNumber(link.name)) {
        int val = Utils.atoi(link.name);
        if (val > highest) highest = val;
      }
      link = link.nextSibling;
    }
    return highest+1;
}

public int getChildren() {
    int n = 0;
    ScriptVarLink link = firstChild;
    while (!Objects.isNull(link)) {
      n++;
      link = link.nextSibling;
    }
    return n;
}

public int getInt() {
    /* strtol understands about hex and octal */
    if (isInt()) return intData;
    if (isNull()) return 0;
    if (isUndefined()) return 0;
    if (isDouble()) return (int)doubleData;
    return 0;
}

public double getDouble() {
    if (isDouble()) return doubleData;
    if (isInt()) return (double)intData;
    if (isNull()) return 0d;
    if (isUndefined()) return 0d;
    return 0; /* or NaN? */
}

public final String getString() {
	
	//signed (int) to string
    if (isInt())
    	return String.valueOf(intData);
    // you gotit, double to string
    if (isDouble())
      return String.valueOf(doubleData);
    
    if (isNull()) return TinyJS.TINYJS_NULL_VAR;
    if (isUndefined()) return TinyJS.TINYJS_UNDEF_VAR;
    // are we just a string here?
    return strData;
}

public void setInt(int val) {
    flags = (flags&~ScriptVarFlags.SCRIPTVAR_VARTYPEMASK) | ScriptVarFlags.SCRIPTVAR_INTEGER;
    intData = val;
    doubleData = 0;
    strData = TinyJS.TINYJS_BLANK_DATA;
}

public void setDouble(double val) {
    flags = (flags&~ScriptVarFlags.SCRIPTVAR_VARTYPEMASK) | ScriptVarFlags.SCRIPTVAR_DOUBLE;
    doubleData = val;
    intData = 0;
    strData = TinyJS.TINYJS_BLANK_DATA;
}

public void setString(final String str) {
    // name sure it's not still a number or integer
    flags = (flags&~ScriptVarFlags.SCRIPTVAR_VARTYPEMASK) | ScriptVarFlags.SCRIPTVAR_STRING;
    strData = str;
    intData = 0;
    doubleData = 0;
}

public void setUndefined() {
    // name sure it's not still a number or integer
    flags = (flags&~ScriptVarFlags.SCRIPTVAR_VARTYPEMASK) | ScriptVarFlags.SCRIPTVAR_UNDEFINED;
    strData = TinyJS.TINYJS_BLANK_DATA;
    intData = 0;
    doubleData = 0;
    removeAllChildren();
}

// this is non-standard behavior, doesn't have to follow mutator protocol
public void setupArray() {
    // name sure it's not still a number or integer
    flags = (flags & ~ScriptVarFlags.SCRIPTVAR_VARTYPEMASK) | ScriptVarFlags.SCRIPTVAR_ARRAY;
    strData = TinyJS.TINYJS_BLANK_DATA;
    intData = 0;
    doubleData = 0;
    strData=null;
    removeAllChildren();
}

public boolean equals(ScriptVar v) {
    ScriptVar resV;
	try {
		resV = mathsOp(v, LexTypes.LEX_EQUAL);
		boolean res = resV.getBool();
	    resV.finalize();
	    return res;
	} catch (Exception e) {
		return false;
	}
    
}

ScriptVar mathsOp(ScriptVar b, int op) throws Exception {
    // Type equality check
    if (op == LexTypes.LEX_TYPEEQUAL || op == LexTypes.LEX_NTYPEEQUAL) {
      // check type first, then call again to check data
      boolean eql = ((this.flags & ScriptVarFlags.SCRIPTVAR_VARTYPEMASK) ==
                  (b.flags & ScriptVarFlags.SCRIPTVAR_VARTYPEMASK));
      if (eql) {
        ScriptVar contents = this.mathsOp(b, LexTypes.LEX_EQUAL);
        if (!contents.getBool()) eql = false;
        if (Objects.isNull(contents.refs)) contents.finalize();
      }
                 ;
      if (op == LexTypes.LEX_TYPEEQUAL)
        return new ScriptVar(eql?1:0);
      else
        return new ScriptVar((!eql)?1:0);
    }
    // do maths...
    if (this.isUndefined() && b.isUndefined()) {
      if (op == LexTypes.LEX_EQUAL) return new ScriptVar(1);
      else if (op == LexTypes.LEX_NEQUAL) return new ScriptVar(0);
      else return new ScriptVar(); // undefined
    } else if ((this.isNumeric() || this.isUndefined()) &&
               (b.isNumeric() || b.isUndefined())) {
        if (!this.isDouble() && !b.isDouble()) {
            // use ints
            int da = this.getInt();
            int db = b.getInt();
            switch (op) {
                case '+': return new ScriptVar(da+db);
                case '-': return new ScriptVar(da-db);
                case '*': return new ScriptVar(da*db);
                case '/': return new ScriptVar(da/db);
                case '&': return new ScriptVar(da&db);
                case '|': return new ScriptVar(da|db);
                case '^': return new ScriptVar(da^db);
                case '%': return new ScriptVar(da%db);
                case LexTypes.LEX_EQUAL:     return new ScriptVar(da==db?1:0);
                case LexTypes.LEX_NEQUAL:    return new ScriptVar(da!=db?1:0);
                case '<':     return new ScriptVar(da<db?1:0);
                case LexTypes.LEX_LEQUAL:    return new ScriptVar(da<=db?1:0);
                case '>':     return new ScriptVar(da>db?1:0);
                case LexTypes.LEX_GEQUAL:    return new ScriptVar(da>=db?1:0);
                default: throw new Exception("Operation "+ScriptLex.getTokenStr(op)+" not supported on the Int datatype");
            }
        } else {
            // use doubles
            double da = this.getDouble();
            double db = b.getDouble();
            switch (op) {
                case '+': return new ScriptVar(da+db);
                case '-': return new ScriptVar(da-db);
                case '*': return new ScriptVar(da*db);
                case '/': return new ScriptVar(da/db);
                case LexTypes.LEX_EQUAL:     return new ScriptVar(da==db?1:0);
                case LexTypes.LEX_NEQUAL:    return new ScriptVar(da!=db?1:0);
                case '<':     return new ScriptVar(da<db?1:0);
                case LexTypes.LEX_LEQUAL:    return new ScriptVar(da<=db?1:0);
                case '>':     return new ScriptVar(da>db?1:0);
                case LexTypes.LEX_GEQUAL:    return new ScriptVar(da>=db?1:0);
                default: throw new Exception("Operation "+ScriptLex.getTokenStr(op)+" not supported on the Double datatype");
            }
        }
    } else if (this.isArray()) {
      /* Just check pointers */
      switch (op) {
           case LexTypes.LEX_EQUAL: return new ScriptVar(this.equals(b)?1:0);
           case LexTypes.LEX_NEQUAL: return new ScriptVar(!this.equals(b)?1:0);
           default: throw new Exception("Operation "+ScriptLex.getTokenStr(op)+" not supported on the Array datatype");
      }
    } else if (this.isObject()) {
          /* Just check pointers */
          switch (op) {
               case LexTypes.LEX_EQUAL: return new ScriptVar(this.equals(b)?1:0);
               case LexTypes.LEX_NEQUAL: return new ScriptVar(!this.equals(b)?1:0);
               default: throw new Exception("Operation "+ScriptLex.getTokenStr(op)+" not supported on the Object datatype");
          }
    } else {
       String da = this.getString();
       String db = b.getString();
       // use strings
       switch (op) {
           case '+':           return new ScriptVar(da+db, ScriptVarFlags.SCRIPTVAR_STRING);
           case LexTypes.LEX_EQUAL:     return new ScriptVar(da.equals(db)?1:0);
           case LexTypes.LEX_NEQUAL:    return new ScriptVar(!da.equals(db)?1:0);
           case '<':     return new ScriptVar(da.compareTo(db)<0?1:0);
           case LexTypes.LEX_LEQUAL:    return new ScriptVar(da.compareTo(db)<=0?1:0);
           case '>':     return new ScriptVar(da.compareTo(db)>0?1:0);
           case LexTypes.LEX_GEQUAL:    return new ScriptVar(da.compareTo(db)>=0?1:0);
           default: throw new Exception("Operation "+ScriptLex.getTokenStr(op)+" not supported on the string datatype");
       }
    }
}

void copySimpleData(ScriptVar val) {
    strData = val.strData;
    intData = val.intData;
    doubleData = val.doubleData;
    flags = (flags & ~ScriptVarFlags.SCRIPTVAR_VARTYPEMASK) | (val.flags & ScriptVarFlags.SCRIPTVAR_VARTYPEMASK);
}

public void copyValue(ScriptVar val) {
    if (!Objects.isNull(val)) {
      copySimpleData(val);
      // remove all current children
      removeAllChildren();
      // copy children of 'val'
      ScriptVarLink child = val.firstChild;
      while (!Objects.isNull(child)) {
        ScriptVar copied;
        // don't copy the 'parent' object...
        if (child.name != TinyJS.TINYJS_PROTOTYPE_CLASS)
          copied = child.var.deepCopy();
        else
          copied = child.var;

        addChild(child.name, copied);

        child = child.nextSibling;
      }
    } else {
      setUndefined();
    }
}

ScriptVar deepCopy() {
    ScriptVar newVar = new ScriptVar();
    newVar.copySimpleData(this);
    // copy children
    ScriptVarLink child = firstChild;
    while (!Objects.isNull(child)) {
        ScriptVar copied;
        // don't copy the 'parent' object...
        if (child.name != TinyJS.TINYJS_PROTOTYPE_CLASS)
          copied = child.var.deepCopy();
        else
          copied = child.var;

        newVar.addChild(child.name, copied);
        child = child.nextSibling;
    }
    return newVar;
}

// called through TinyJS.trace()
public void trace() {
	trace("","");
}

public void trace(String name) {
	trace("",name);
}

public void trace(String indentStr, final String name) {
    System.out.printf("%s'%s' = '%s' %s\n",
        indentStr,
        name,
        getString(),
        getFlagsAsString());
    String indent = indentStr+" ";
    ScriptVarLink link = firstChild;
    
    while (!Objects.isNull(link)) {
      link.var.trace(indent, link.name);
      link = link.nextSibling;
    }
}


// make a space delimited string from coded data types
String getFlagsAsString() {
  String flagstr = "";
  if ((flags&ScriptVarFlags.SCRIPTVAR_FUNCTION)>0) flagstr = flagstr + ScriptVarFlags.SCRIPTSTR_FUNCTION;
  if ((flags&ScriptVarFlags.SCRIPTVAR_OBJECT)>0) flagstr = flagstr + ScriptVarFlags.SCRIPTSTR_OBJECT;
  if ((flags&ScriptVarFlags.SCRIPTVAR_ARRAY)>0) flagstr = flagstr + ScriptVarFlags.SCRIPTSTR_ARRAY;
  if ((flags&ScriptVarFlags.SCRIPTVAR_NATIVE)>0) flagstr = flagstr + ScriptVarFlags.SCRIPTSTR_NATIVE;
  if ((flags&ScriptVarFlags.SCRIPTVAR_DOUBLE)>0) flagstr = flagstr + ScriptVarFlags.SCRIPTSTR_DOUBLE;
  if ((flags&ScriptVarFlags.SCRIPTVAR_INTEGER)>0) flagstr = flagstr + ScriptVarFlags.SCRIPTSTR_INTEGER;
  if ((flags&ScriptVarFlags.SCRIPTVAR_STRING)>0) flagstr = flagstr + ScriptVarFlags.SCRIPTSTR_STRING;
  return flagstr;
}

String getParsableString() {
  // Numbers can just be put in directly
  if (isNumeric())
    return getString();
  if (isFunction()) {
    String funcStr = "function (";
    // get list of parameters
    ScriptVarLink link = firstChild;
    while (!Objects.isNull(link)) {
      funcStr += link.name;
      if (!Objects.isNull(link.nextSibling)) funcStr += ",";
      link = link.nextSibling;
    }
    // add function body
    funcStr += ") " + getString();
    return funcStr;
  }
  // if it is a string then we quote it
  if (isString())
    return Utils.getJSString(getString());
  if (isNull())
      return TinyJS.TINYJS_NULL_VAR;
  return TinyJS.TINYJS_UNDEF_VAR;
}

public String getJSON() {
	return getJSON("");
}

public String getJSON(final String linePrefix) {
   // append a json formatted entry
	String destination = "";
	if (isObject()) {
      String indentedLinePrefix = linePrefix + "  ";
      // children - handle with bracketed list
      destination += "{ \n";
      ScriptVarLink link = firstChild;
      while (!Objects.isNull(link)) {
        destination += indentedLinePrefix;
        destination  += Utils.getJSString(link.name);
        destination  += " : ";
        destination += link.var.getJSON(indentedLinePrefix);
        link = link.nextSibling;
        if (!Objects.isNull(link)) {
          destination  += ",\n";
        }
      }
      destination += "\n" + linePrefix + "}";
    } else if (isArray()) {
      String indentedLinePrefix = linePrefix + "  ";
      destination += "[\n";
      int len = getArrayLength();
      if (len>10000) len=10000; // we don't want to get stuck here!

      for (int i=0;i<len;i++) {
        destination += getArrayIndex(i).getJSON(indentedLinePrefix);
        if (i<len-1) destination += ",\n";
      }

      destination += "\n" + linePrefix + "]";
    } else {
      // no children or a function... just write value directly
      destination += getParsableString();
    }
	return destination;
}


void setCallback(Method callback, String userdata) {
    jsCallback = callback;
    jsCallbackUserData = userdata;
}

public ScriptVar ref() {
    refs++;
    return this;
}

public void unref() {
    //if (refs<=0) print("OMFG, we have unreffed too far!\n");
    if ((--refs)==0) {
      this.finalize();
    }
}

public int getRefs() {
    return refs;
}

}
