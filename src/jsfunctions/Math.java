package jsfunctions;

import frenchvanilla.ScriptVar;

public class Math {

public static void Rand(ScriptVar c) {
    c.getReturnVar().setDouble(java.lang.Math.random());
}

public static void RandInt(ScriptVar c) {
    c.getReturnVar().setInt((int)java.lang.Math.random());
}
	
//Math.abs(x) - returns absolute of given value
public static void Abs(ScriptVar c, String userdata) {
    if ( c.getParameter("a").isInt() ) {
    	//((a)>=0 ? (a) : (-(a)))
    	int val = c.getParameter("a").getInt();
    	val=val<0?val*(-1):val;
      c.getReturnVar().setInt( val );
    } else if ( c.getParameter("a").isDouble() ) {
    	double val = c.getParameter("a").getInt();
    	val=val<0?val*(-1):val;
      c.getReturnVar().setDouble( val );
    }
}

//Math.round(a) - returns nearest round of given value
public static void Round(ScriptVar c, String userdata) {
	// if its an int, do nothing, its already rounded
    if ( c.getParameter("a").isDouble() ) {
    	 double val = c.getParameter("a").getDouble();
    	 // scientific rounding
    	 val += 0.4444444444444444444444444444445D;
      c.getReturnVar().setDouble( (double)((int)val) );
    }
}

//Math.min(a,b) - returns minimum of two given values 
public static void Min(ScriptVar c, String userdata) {
    if ( c.getParameter("a").isInt() && (c.getParameter("b").isInt()) ) {
      c.getReturnVar().setInt( Integer.compare(c.getParameter("a").getInt(), c.getParameter("b").getInt() ) < 0 ? c.getParameter("a").getInt() : c.getParameter("b").getInt() );
    } else {
      c.getReturnVar().setDouble( Double.compare(c.getParameter("a").getDouble(), c.getParameter("b").getDouble() ) < 0 ? c.getParameter("a").getDouble() : c.getParameter("b").getDouble() );
    }
}

//Math.max(a,b) - returns maximum of two given values  
public static void Max(ScriptVar c, String userdata) {
    if ( (c.getParameter("a").isInt()) && (c.getParameter("b").isInt()) ) {
      c.getReturnVar().setInt( Integer.compare(c.getParameter("a").getInt(), c.getParameter("b").getInt() ) >= 0 ? c.getParameter("a").getInt() : c.getParameter("b").getInt() );
    } else {
      c.getReturnVar().setDouble( Double.compare(c.getParameter("a").getDouble(), c.getParameter("b").getDouble() ) >= 0 ? c.getParameter("a").getDouble() : c.getParameter("b").getDouble() );
    }
}

//Math.range(x,a,b) - returns value limited between two given values : (a,min,max)((a)<(min) ? min : ((a)>(max) ? max : a ))
public static void Range(ScriptVar c, String userdata) {
    if ( c.getParameter("x").isInt() ) {
    	int val = c.getParameter("x").getInt();
    	int min = c.getParameter("a").getInt();
    	int max = c.getParameter("b").getInt();
      c.getReturnVar().setInt( val >= min && val <= max ? val : val < min ? min : max );
    } else {
    	double val = c.getParameter("x").getDouble();
    	double min = c.getParameter("a").getDouble();
    	double max = c.getParameter("b").getDouble();
      c.getReturnVar().setDouble( val >= min && val <= max ? val : val < min ? min : max );
    }
}

//Math.sign(a) - returns sign of given value (-1==negative,0=zero,1=positive)
public static void Sign(ScriptVar c, String userdata) {
    if ( c.getParameter("a").isInt() ) {
      c.getReturnVar().setInt( c.getParameter("a").getInt() == 0 ? 0 : (c.getParameter("a").getInt() > 0 ? 1 : -1) );
    } else if ( c.getParameter("a").isDouble() ) {
      c.getReturnVar().setDouble( c.getParameter("a").getDouble() == 0 ? 0 : (c.getParameter("a").getDouble()>0?1:-1) );
    }
}

//Math.E() - returns E Neplero value
public static void E(ScriptVar c, String userdata) {
  c.getReturnVar().setDouble(java.lang.Math.E);
}

//Math.PI() - returns PI value
public static void PI(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble(java.lang.Math.PI);
}

//Math.toDegrees(a) - returns degree value of a given angle in radians
public static void ToDegrees(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( (180.0/java.lang.Math.PI)*( c.getParameter("a").getDouble() ) );
}

//Math.toRadians(a) - returns radians value of a given angle in degrees
public static void ToRadians(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( (java.lang.Math.PI/180.0)*( c.getParameter("a").getDouble() ) );
}

//Math.sin(a) - returns trig. sine of given angle in radians
public static void Sin(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.sin( c.getParameter("a").getDouble() ) );
}

//Math.asin(a) - returns trig. arcsine of given angle in radians
public static void ASin(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.asin( c.getParameter("a").getDouble() ) );
}

//Math.cos(a) - returns trig. cosine of given angle in radians
public static void Cos(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.cos( c.getParameter("a").getDouble() ) );
}

//Math.acos(a) - returns trig. arccosine of given angle in radians
public static void ACos(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.acos( c.getParameter("a").getDouble() ) );
}

//Math.tan(a) - returns trig. tangent of given angle in radians
public static void Tan(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.tan( c.getParameter("a").getDouble() ) );
}

//Math.atan(a) - returns trig. arctangent of given angle in radians
public static void ATan(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.atan( c.getParameter("a").getDouble() ) );
}

//Math.sinh(a) - returns trig. hyperbolic sine of given angle in radians
public static void Sinh(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.sinh( c.getParameter("a").getDouble() ) );
}

//Math.asinh(a) - returns trig. hyperbolic arcsine of given angle in radians; ln(x + sqrt(x^2 + 1))
public static void ASinh(ScriptVar c, String userdata) {
	double val = c.getParameter("a").getDouble();
    c.getReturnVar().setDouble( java.lang.Math.log(val+ java.lang.Math.sqrt(java.lang.Math.pow(val, 2)+1) ) );
}

//Math.cosh(a) - returns trig. hyperbolic cosine of given angle in radians
public static void Cosh(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.cosh( c.getParameter("a").getDouble() ) );
}

//Math.acosh(a) - returns trig. hyperbolic arccosine of given angle in radians; ln(sqrt(x^2 - 1) + x)
public static void ACosh(ScriptVar c, String userdata) {
	double val = c.getParameter("a").getDouble();
    c.getReturnVar().setDouble( java.lang.Math.log(val+ java.lang.Math.sqrt(java.lang.Math.pow(val, 2)-1) ) );
}

//Math.tanh(a) - returns trig. hyperbolic tangent of given angle in radians
public static void Tanh(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.tanh( c.getParameter("a").getDouble() ) );
}

//Math.atan(a) - returns trig. hyperbolic arctangent of given angle in radians, ln((1 + x)/(1 - x)) / 2
public static void ATanh(ScriptVar c, String userdata) {
	double val = c.getParameter("a").getDouble();
    c.getReturnVar().setDouble( java.lang.Math.log((val+1)/(1-val)) / 2 );
}

//Math.log(a) - returns natural logaritm (base E) of given value
public static void Log(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.log( c.getParameter("a").getDouble() ) );
}

//Math.log10(a) - returns logaritm(base 10) of given value
public static void Log10(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.log10( c.getParameter("a").getDouble() ) );
}

//Math.exp(a) - returns e raised to the power of a given number
public static void Exp(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.exp( c.getParameter("a").getDouble() ) );
}

//Math.pow(a,b) - returns the result of a number raised to a power (a)^(b)
public static void Pow(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.pow( c.getParameter("a").getDouble(), c.getParameter("b").getDouble() ) );
}

//Math.sqr(a) - returns square of given value
public static void Sqr(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.pow(c.getParameter("a").getDouble(), 2) );
}

//Math.sqrt(a) - returns square root of given value
public static void Sqrt(ScriptVar c, String userdata) {
    c.getReturnVar().setDouble( java.lang.Math.sqrt( c.getParameter("a").getDouble() ) );
}
	
} // class
