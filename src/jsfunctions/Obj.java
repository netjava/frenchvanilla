package jsfunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import frenchvanilla.ScriptVar;
import frenchvanilla.ScriptVarLink;
import frenchvanilla.TinyJS;

// this group of functions could be in thier own classifications
public class Obj {
	
	void Trace(ScriptVar c, TinyJS js) {
	    js.root.trace();
	}

	void ObjectDump(ScriptVar c) {
	    c.getParameter("this").trace("> ");
	}

	void ObjectClone(ScriptVar c) {
	    ScriptVar obj = c.getParameter("this");
	    c.getReturnVar().copyValue(obj);
	}


	void CharToInt(ScriptVar c) {
		c.getReturnVar().setInt((int)(c.getParameter("ch").getString().toCharArray()[0]));
	}
	
	
	void StringIndexOf(ScriptVar c) {
	    c.getReturnVar().setInt(c.getParameter("this").getString().indexOf(c.getParameter("search").getString()));
	}

	void StringSubstring(ScriptVar c) {
		// lo and hi are the sub-bounds of the string
	    int lo = c.getParameter("lo").getInt();
	    int hi = c.getParameter("hi").getInt();
	    if(lo>=0 && hi-lo>0) {
	    	c.getReturnVar().setString(c.getParameter("this").getString().substring(lo, hi));
	    } else c.getReturnVar().setString("");
	}

	void StringCharAt(ScriptVar c) {
	    String str = c.getParameter("this").getString();
	    int pos = c.getParameter("pos").getInt();
	    if (pos>=0 && pos<str.length())
	      c.getReturnVar().setString(str.substring(pos,pos+1));
	    else
	      c.getReturnVar().setString("");
	}

	// duplicate of charAt, but assign to an int
	void StringCharCodeAt(ScriptVar c) {
	    String str = c.getParameter("this").getString();
	    int pos = c.getParameter("pos").getInt();
	    if (pos>=0 && pos<str.length())
	      c.getReturnVar().setInt((int)str.charAt(pos));
	    else
	      c.getReturnVar().setInt(0);
	}
	
	// assign the tokens, delimited by 'separator', to an array
	void StringSplit(ScriptVar c) {
		String str = c.getParameter("this").getString();
	    String sep = c.getParameter("separator").getString();
	    ScriptVar rtn = c.getReturnVar();
	    rtn.setupArray();
	    int i = 0;
	    for(String s: str.split(sep)) 
	    	rtn.setArrayIndex(i++, new ScriptVar(s));
	    // the c version catches a trailing token here
	}

	void StringFromCharCode(ScriptVar c) {
		c.getReturnVar().setString(String.valueOf((char)c.getParameter("char").getInt()));
	}
/*  THE LIB NEEDS HARDENING BEFORE MAKING ADDITIONS
	// make a stack of objects
	private void Push(ScriptVar c) {
		ScriptVar rtn = c.getReturnVar();
		if(!c.getParameter("this").isArray()) rtn.setupArray();
		rtn.setArrayIndex(c.getChildren(), c.getParameter("this"));
	}
	*/
	void ArrayContains(ScriptVar c) {
		  ScriptVar obj = c.getParameter("obj");
		  ScriptVarLink v = c.getParameter("this").firstChild;
		  boolean contains = false;
		  while (!Objects.isNull(v)) {
		      if (v.var.equals(obj)) {
		        contains = true;
		        break;
		      }
		      v = v.nextSibling;
		  }
		  c.getReturnVar().setInt(contains?1:0);
	}

	void ArrayRemove(ScriptVar c) {
		ScriptVar obj = c.getParameter("obj");
		  List<Integer> removedIndices = new ArrayList<Integer>();
		  ScriptVarLink v;
		  // remove
		  v = c.getParameter("this").firstChild;
		  while (!Objects.isNull(v)) {
		      if (v.var.equals(obj)) {
		        removedIndices.add(v.getIntName());
		      }
		      v = v.nextSibling;
		  }
		  // renumber
		  v = c.getParameter("this").firstChild;
		  while (!Objects.isNull(v)) {
		      int n = v.getIntName();
		      int newn = n;
		      // added the following condition
		      if(!Objects.isNull(removedIndices))
			      for (int i=0;i<removedIndices.size();i++)
			        if (n>= removedIndices.get(i))
			          newn--;
			      if (newn!=n)
			        v.setIntName(newn);
			      v = v.nextSibling;
		  }
	}

	void ArrayJoin(ScriptVar c) {
		  String sep = c.getParameter("separator").getString();
		  ScriptVar arr = c.getParameter("this");

		  String sstr=new String("");
		  int l = arr.getArrayLength();
		  for (int i=0;i<l;i++) {
		    if (i>0) sstr += sep;
		    sstr += arr.getArrayIndex(i).getString();
		  }

		  c.getReturnVar().setString(sstr);
	}
	
	
	
	void IntegerParseInt(ScriptVar c) {
	    c.getReturnVar().setInt(Integer.parseInt(c.getParameter("str").getString()));
	}

	void IntegerValueOf(ScriptVar c) {
		c.getReturnVar().setInt((int)c.getParameter("str").getString().charAt(0));
	}

	
	
	void JSONStringify(ScriptVar c) {
	    c.getReturnVar().setString(c.getParameter("obj").getJSON());
	}

	void Exec(ScriptVar c, TinyJS tinyjs) {
	    try {
			tinyjs.execute(c.getParameter("jsCode").getString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void Eval(ScriptVar c, TinyJS tinyjs) {
	    try {
			c.setReturnVar(tinyjs.evaluateComplex(c.getParameter("jsCode").getString()).var);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
