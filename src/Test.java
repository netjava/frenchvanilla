import java.io.File;
import java.io.FileReader;
import java.nio.CharBuffer;
import frenchvanilla.*;

public class Test {
	// Auto-generated method stub
	public static void main(String[] args) {
		final int numTests=36;
		
		  TinyJS s= new TinyJS();

		  try {
			registerFunctions(s);
			registerMathFunctions(s);
		} catch (NoSuchMethodException | SecurityException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		  s.root.addChild("result", new ScriptVar("0",frenchvanilla.ScriptVarFlags.SCRIPTVAR_INTEGER));
		  /*
		  try {
			  // load the files into a buffer
			  FileReader fr;
			  CharBuffer cb;
			  for (int i=1;i<numTests;i++) {
				 String strTestName="test"+String.format("%03d", i)+".js\n";
				 System.out.printf("testing file %s",strTestName);
				 File file= new File(strTestName);
				 fr = new FileReader(file);
				 cb= CharBuffer.allocate((int) file.length());
				 fr.read(cb);
				 String buffer = cb.toString();
				 System.out.printf("buffer: %s", buffer);
				 //s.execute(buffer);
			  }
		  } catch (Exception e) {
		    System.out.printf("ERROR: %s\n", e.getMessage());
		  }
		  */
		  if (s.root.getParameter("result").getBool())
			  System.out.printf("PASS\n");
		  else {
			// write the symbols to the screen
		      String symbols = s.root.getJSON();
		      //fprintf(f, "%s", symbols.str().c_str());
		      System.out.printf("FAIL\nSymbols output %s\n",symbols);
		  }

	} // Test class

	static void registerFunctions(TinyJS tinyJS) throws NoSuchMethodException, SecurityException{
	    tinyJS.addNative("function exec(jsCode)", jsfunctions.Obj.class.getDeclaredMethod("Exec", ScriptVar.class, TinyJS.class), tinyJS.toString()); // execute the given code
	    tinyJS.addNative("function eval(jsCode)", jsfunctions.Obj.class.getDeclaredMethod("Eval", ScriptVar.class, TinyJS.class), tinyJS.toString()); // execute the given string (an expression) and return the result
	    tinyJS.addNative("function trace()", jsfunctions.Obj.class.getDeclaredMethod("Trace", ScriptVar.class, TinyJS.class), tinyJS.toString());
	    tinyJS.addNative("function Object.dump()", jsfunctions.Obj.class.getDeclaredMethod("ObjectDump", ScriptVar.class), "");
	    tinyJS.addNative("function Object.clone()", jsfunctions.Obj.class.getDeclaredMethod("ObjectClone", ScriptVar.class), "");
	    tinyJS.addNative("function charToInt(ch)", jsfunctions.Obj.class.getDeclaredMethod("CharToInt", ScriptVar.class), ""); //  convert a character to an int - get its value
	    tinyJS.addNative("function String.indexOf(search)", jsfunctions.Obj.class.getDeclaredMethod("StringIndexOf", ScriptVar.class), ""); // find the position of a string in a string, -1 if not
	    tinyJS.addNative("function String.substring(lo,hi)", jsfunctions.Obj.class.getDeclaredMethod("StringSubstring", ScriptVar.class), "");
	    tinyJS.addNative("function String.charAt(pos)", jsfunctions.Obj.class.getDeclaredMethod("StringCharAt", ScriptVar.class), "");
	    tinyJS.addNative("function String.charCodeAt(pos)", jsfunctions.Obj.class.getDeclaredMethod("StringCharCodeAt", ScriptVar.class), "");
	    tinyJS.addNative("function String.fromCharCode(char)", jsfunctions.Obj.class.getDeclaredMethod("StringFromCharCode", ScriptVar.class), "");
	    tinyJS.addNative("function String.split(separator)", jsfunctions.Obj.class.getDeclaredMethod("StringSplit", ScriptVar.class), "");
	    tinyJS.addNative("function Integer.parseInt(str)", jsfunctions.Obj.class.getDeclaredMethod("IntegerParseInt", ScriptVar.class), ""); // string to int
	    tinyJS.addNative("function Integer.valueOf(str)", jsfunctions.Obj.class.getDeclaredMethod("IntegerValueOf", ScriptVar.class), ""); // value of a single character
	    tinyJS.addNative("function JSON.stringify(obj, replacer)", jsfunctions.Obj.class.getDeclaredMethod("JSONStringify", ScriptVar.class), ""); // convert to JSON. replacer is ignored at the moment
	    // JSON.parse is left out as you can (unsafely!) use eval instead
	    tinyJS.addNative("function Array.contains(obj)", jsfunctions.Obj.class.getDeclaredMethod("ArrayContains", ScriptVar.class), "");
	    tinyJS.addNative("function Array.remove(obj)", jsfunctions.Obj.class.getDeclaredMethod("ArrayRemove", ScriptVar.class), "");
	    tinyJS.addNative("function Array.join(separator)", jsfunctions.Obj.class.getDeclaredMethod("ArrayJoin", ScriptVar.class), "");
	}
	
	
	static void registerMathFunctions(TinyJS tinyJS) throws NoSuchMethodException, SecurityException {     
	    // --- Math and Trigonometry functions ---
	    tinyJS.addNative("function Math.abs(a)", jsfunctions.Math.class.getDeclaredMethod("Abs", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.round(a)", jsfunctions.Math.class.getDeclaredMethod("Round", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.min(a,b)", jsfunctions.Math.class.getDeclaredMethod("Min", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.max(a,b)", jsfunctions.Math.class.getDeclaredMethod("Max", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.range(x,a,b)", jsfunctions.Math.class.getDeclaredMethod("Range", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.sign(a)", jsfunctions.Math.class.getDeclaredMethod("Sign", ScriptVar.class, String.class), "");
	    
	    tinyJS.addNative("function Math.PI()", jsfunctions.Math.class.getDeclaredMethod("PI", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.toDegrees(a)", jsfunctions.Math.class.getDeclaredMethod("ToDegrees", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.toRadians(a)", jsfunctions.Math.class.getDeclaredMethod("ToRadians", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.sin(a)", jsfunctions.Math.class.getDeclaredMethod("Sin", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.asin(a)", jsfunctions.Math.class.getDeclaredMethod("ASin", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.cos(a)", jsfunctions.Math.class.getDeclaredMethod("Cos", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.acos(a)", jsfunctions.Math.class.getDeclaredMethod("ACos", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.tan(a)", jsfunctions.Math.class.getDeclaredMethod("Tan", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.atan(a)", jsfunctions.Math.class.getDeclaredMethod("ATan", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.sinh(a)", jsfunctions.Math.class.getDeclaredMethod("Sinh", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.asinh(a)", jsfunctions.Math.class.getDeclaredMethod("ASinh", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.cosh(a)", jsfunctions.Math.class.getDeclaredMethod("Cosh", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.acosh(a)", jsfunctions.Math.class.getDeclaredMethod("ACosh", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.tanh(a)", jsfunctions.Math.class.getDeclaredMethod("Tanh", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.atanh(a)", jsfunctions.Math.class.getDeclaredMethod("ATanh", ScriptVar.class, String.class), "");
	       
	    tinyJS.addNative("function Math.E()", jsfunctions.Math.class.getDeclaredMethod("E", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.log(a)", jsfunctions.Math.class.getDeclaredMethod("Log", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.log10(a)", jsfunctions.Math.class.getDeclaredMethod("Log10", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.exp(a)", jsfunctions.Math.class.getDeclaredMethod("Exp", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.pow(a,b)", jsfunctions.Math.class.getDeclaredMethod("Pow", ScriptVar.class, String.class), "");
	    
	    tinyJS.addNative("function Math.sqr(a)", jsfunctions.Math.class.getDeclaredMethod("Sqr", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.sqrt(a)", jsfunctions.Math.class.getDeclaredMethod("Sqrt", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.rand()",jsfunctions.Math.class.getDeclaredMethod("Rand", ScriptVar.class, String.class), "");
	    tinyJS.addNative("function Math.randInt(min, max)",jsfunctions.Math.class.getDeclaredMethod("RandInt", ScriptVar.class, String.class), "");
	}
	
}
